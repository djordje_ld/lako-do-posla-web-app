function endpointRequiresToken(route) {
    let isCandidateRoute = route.indexOf('api/user/') !== -1
    let isEmployerRoute = route.indexOf('api/employer/') !== -1
    let isRefreshTokenRoute = route === 'api/token/refresh';

    return (isCandidateRoute || isEmployerRoute || isRefreshTokenRoute)
}

let USER_TYPE;
let REFRESH_TOKEN_ACTION;
let LOGOUT_ACTION;

function setMainVars(store) {
    USER_TYPE = store.state.common.user_type;
    REFRESH_TOKEN_ACTION = USER_TYPE+'/auth/refresh';
    LOGOUT_ACTION = USER_TYPE+'/auth/logout';
}

export default function ({ store, app: { $axios }, redirect }) {
    // user_type = candidate / employer


    // const IGNORED_PATHS = ['api/public/categories' ];
    // const isIgnored = IGNORED_PATHS.some(path => a.includes(path))

    $axios.onRequest((config) => {
        setMainVars(store)
        // check if the user is authenticated
        if (USER_TYPE && store.state[USER_TYPE].auth.token) {
            // set the Authorization header using the access token
            config.headers.Authorization = 'Bearer ' + store.state[USER_TYPE].auth.token
            config.headers.Cookie = 'candidate-saved-jobs=value'

        }
        return config
    })
    $axios.onResponse(response => {
        setMainVars(store)
    })
    $axios.onError((error) => {
        setMainVars(store)

        store.dispatch('overlayLoader/toggleOverlayLoader', false);
        return new Promise(async (resolve, reject) => {
            // ignore certain paths (i.e. paths relating to authentication)

            // get the status code from the response
            const statusCode = error.response ? error.response.status : -1
            const errorMessage = error.response ? error.response.data.message : '';
            // only handle authentication errors or errors involving the validity of the token
            if (statusCode === 401 && errorMessage === 'Expired token') {

                // get the refresh token from the state if it exists

                const refreshToken = store.state[USER_TYPE].auth.refresh_token

                if (refreshToken) {

                    try {
                        await store.dispatch(REFRESH_TOKEN_ACTION)

                        return resolve($axios(error.config))
                    } catch (e) {
                        await store.dispatch(LOGOUT_ACTION)
                        return redirect('/')
                    }
                } else {
                    await store.dispatch(LOGOUT_ACTION)
                    return redirect('/')
                }
            }
            // {"candidate":{"auth":{"token":null,"refresh_token":null,"expires":null},"profile":{"data":{}}},"employer":{"auth":{"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2NTY2NzY2NTgsImV4cCI6MTY1Njc2MzA1OCwiZW1wX2lkIjoiMjM1MiJ9._IwAX7pMlRsOmsUJNo1GZtBuPC8jdS5L-hUDfj3D-PI","refresh_token":"b3ab058e40336db90a4a3a30c58e0622","expires":1656856182},"profile":{"data":{"id":"2352","username":"konkurs@lakodoposla.com","company":"Talentmarket d.o.o.","company_description":"<p>Magical</p>\n<p>Tekst za opis kompanije</p>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>","address":"Bulevar Arsenija Čarnojevića 52a, VI/17","location":"Beogradddd","postal":"11070","pib":"1049829941111","phone":"011/314-84-22","fax":"011/314-84-01","website":"http://www.lakodoposla.com","active":"1","logo":"https://lakodoposla.com/image.php?id=115526940","hot_logo":"https://lakodoposla.com/image.php?id=93180640","contacts":[{"id":"5336","name":"darko","email":"darko_ljubic@yahoo.com","phone":""},{"id":"5975","name":"email@address.milevolidisko","email":"email@address.milevolidisko","phone":null},{"id":"6403","name":"georgiepaun@gmail.com","email":"georgiepaun@gmail.com","phone":null}]}}},"common":{"user_type":"employer","theme_dark":false}}
            // ignore all other errors, let component or other error handlers handle them
            return reject(error)
        })
    })

}