import createPersistedState from 'vuex-persistedstate'
import * as Cookies from 'js-cookie'
import cookie from 'cookie'

export default ({ store, req, isDev }) => {
    createPersistedState({
        key: 'candidate-auth', // choose any name for your cookie
        paths: [
            'candidate.auth.token',
            'candidate.auth.refresh_token',
            'candidate.auth.expires',
        ],
        storage: {
            getItem: key => process.client ? JSON.parse(Cookies.get(key) ? Cookies.get(key) : '{}') : cookie.parse(req.headers.cookie || '')[key],
            setItem: (key, value) => Cookies.set(key, value, { expires: 14, secure: !isDev }),
            removeItem: key => Cookies.remove(key)
        }
    })(store)
}