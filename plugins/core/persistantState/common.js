import createPersistedState from 'vuex-persistedstate'
import * as Cookies from 'js-cookie'
import cookie from 'cookie'

export default ({ store, req, isDev }) => {
    createPersistedState({
        key: 'common-stuff', // choose any name for your cookie
        paths: [
            'common.user_type',
            'common.theme_dark'
        ],
        storage: {
            getItem: key => process.client ? JSON.parse(Cookies.get(key) ? Cookies.get(key) : '{}') : cookie.parse(req.headers.cookie || '')[key],
            setItem: (key, value) => Cookies.set(key, value, { expires: 14, secure: !isDev }),
            removeItem: key => Cookies.remove(key)
        }
    })(store)
}