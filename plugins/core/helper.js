import Vue from "vue";

const helper = {
    capitalize(string) {
        return string && string[0].toUpperCase() + string.slice(1);
    },
    validateEmail(email) {
        let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(String(email).toLowerCase());
    },
    decodeHTML:function (html) {
        var txt = document.createElement('textarea');
        txt.innerHTML = html;

        var txt2 = document.createElement('textarea');
        txt2.innerHTML = txt.value;

        return txt2.value;
    }

}

export default ({ app }, inject) => {
    inject('helper', helper)
}
