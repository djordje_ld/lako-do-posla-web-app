import Vue from 'vue';

function htmlDecode(input) {
    let doc = new DOMParser().parseFromString(input, "text/html");
    return doc.documentElement.textContent;
}

Vue.filter('html_decode', htmlDecode)