import Vue from 'vue';

function ucfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

Vue.filter('ucfirst', ucfirst)