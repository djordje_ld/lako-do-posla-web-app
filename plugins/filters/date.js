import Vue from 'vue';

function date(unixTimestamp) {
    if (!unixTimestamp) return 'nije setovano/not set';
    let months = {
        0:{
            full:'januar',
            short: 'jan'
        },
        1:{
            full:'februar',
                short: 'feb'
        },
        2:{
            full:'mart',
                short: 'mar'
        },
        3:{
            full:'april',
                short: 'apr'
        },
        4:{
            full:'maj',
                short: 'maj'
        },
        5:{
            full:'jun',
                short: 'jun'
        },
        6:{
            full:'jul',
                short: 'jul'
        },
        7:{
            full:'avgust',
                short: 'avg'
        },
        8:{
            full:'septembar',
                short: 'sep'
        },
        9:{
            full:'oktobar',
            short: 'okt'
        },
        10:{
            full:'novembar',
                short: 'nov'
        },
        11:{
            full:'decembar',
                short: 'dec'
        },
    };
    let newDate = new Date(unixTimestamp*1000);
    let month = newDate.getMonth();
    let short_month = months[month].short+'.';
    //return newDate.getDate() + ". " + short_month + " '"+ newDate.getFullYear().toString().substring(2,4) ;
    return newDate.getDate() + ". " + short_month + " "+ newDate.getFullYear();
}

Vue.filter('date', date)