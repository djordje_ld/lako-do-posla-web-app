import Vue from 'vue';

function clearRouteString(string) {
    let arr = string.split('__');
    return arr[0];
}

Vue.filter('clearRouteString', clearRouteString)