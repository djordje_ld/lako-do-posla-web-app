import sr from '../languages/sr.js';
import en from '../languages/en.js';
import multi_language_routes from '../languages/routes/config';

export const I18n = {
    locales: [
        {code: 'sr', iso: 'sr-SR'},
        {code: 'en', iso: 'en-US'}
    ],
    defaultLocale: 'sr',
    detectBrowserLanguage: false,
    vueI18n: {
        fallbackLocale: 'sr',
        messages: {sr, en}
    },
    parsePages: false,
    pages: multi_language_routes
};