    const multiselectFix = () => {

        const scrollFix = () => { //compare some search with the current one
            const container = document.querySelector('.menuable__content__active')
            let lastPosition = container.scrollTop;

            let fun = function () {
                container.scrollTop = lastPosition;
                container.removeEventListener('scroll', fun)
            }

            container.addEventListener('scroll', fun)
        };
        return { scrollFix }
    }

    export default multiselectFix;