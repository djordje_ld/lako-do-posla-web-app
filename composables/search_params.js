import {ref} from '@vue/composition-api'
const searchParams = () => {

    //emp_id=&regions=&categories=&search_term=&career_levels=&education_levels=&date_range=0

    const searchParamsObject = ref({
        emp_id: '',
        regions: '',
        categories: '',
        search_term: '',
        career_levels: '',
        education_levels: '',
        date_range: '',
        page: 1,
        sort: '',
    });

    const getSearchParam = (param, asArray = false) => {
        if (asArray) {
            return searchParamsObject.value[param].length ? searchParamsObject.value[param].split(',') : []
        } else {
            return  searchParamsObject.value[param];
        }
    }

    const setSearchParams = (searchParamsString) => { //let's say searchParamsString = search_term=kuvar&page=2
        if (searchParamsString) {
            let explodedParams = searchParamsString.split('&'); // you'll get array // [search_term=kuvar, page=2]


            for (let i in explodedParams) { //loop  through [search_term=kuvar, page=2]
                let explodedValues = explodedParams[i].split('='); // you'll get [search_term, kuvar]
                searchParamsObject.value[explodedValues[0]] = explodedValues[1] //it literally searchParamsObject.value.search_term = kuvar
            }
        }
    };

    const getSearchParamsString = () => {
        let objectKeys = Object.keys(searchParamsObject.value);
        let objectValues = Object.values(searchParamsObject.value);
        let searchString = [];
        for (let i in objectKeys) {
            searchString.push(objectKeys[i] + '=' + objectValues[i]);
        }
        return searchString.join('&');
    };

    const getSearchParamsObject = () => {
        return searchParamsObject.value;
    };


    const setSearchObjectProp = (prop, value) => {
        //only one value allowed (radio)
        if (prop == 'date_range') {
            searchParamsObject.value[prop] = value;
            return;
        }
        // multiple values allowed (checkbox)
        let selectedData = (searchParamsObject.value[prop] == '') ?  [] : searchParamsObject.value[prop].split(',');
        if (selectedData.indexOf(String(value)) > -1) {
            selectedData = selectedData.filter(data => {
                return data != String(value);
            })

        } else {
            selectedData.push(value)
        }
        searchParamsObject.value[prop] = selectedData.sort().join(',')
    };

    const compareSearches = (search) => { //compare some search with the current one
        let diff = false;
        let currentSearch = getSearchParamsObject();
        let searchProps = Object.keys(search);
        for (let prop in searchProps) {
            if (search[prop] != currentSearch[prop]) {
                diff = true;
                break;
            }
        }
        return diff;
    };
    return { searchParamsObject, setSearchParams, getSearchParamsString, getSearchParamsObject, setSearchObjectProp, getSearchParam, compareSearches }
}

export default searchParams;