export default async function (context) {
    if ((!context.route.query.mod && context.route.query.mod != 'reset_password') || !context.route.query.uid) {
        context.redirect(context.localePath('/'))
    }
}