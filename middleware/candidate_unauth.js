// this middleware will pass only if user is NOT authenticated.
// E.G. we don't want to show Login page to a logged in user.
export default async function (context) {
    if (context.store.getters['candidate/auth/isAuthenticated']) {
        context.redirect(context.localePath('/'))
    }
}