export default async function (context) {
    if (!context.store.getters['candidate/auth/isAuthenticated']) {
        context.redirect(context.localePath('candidate-login'))
    }
}