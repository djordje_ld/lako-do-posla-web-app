export default async function (context) {
    context.app.router.afterEach((to, from) => {
        if (from.fullPath !== to.fullPath) {
            setTimeout(() => window.scrollTo({ top: 0, behavior: 'smooth' }), 500)
        }
    })
}