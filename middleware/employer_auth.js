export default async function (context) {
    if (!context.store.getters['employer/auth/isAuthenticated']) {
        context.redirect(context.localePath('employer-login'))
    }
}