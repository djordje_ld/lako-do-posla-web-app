import candidate_routes from './candidate';
import employer_routes from './employer';

export default {
    ...candidate_routes,
    ...employer_routes,
    '/': {
        sr: '/',
        en: '/',
    },
    'about-us/index': {
        sr: '/o-nama',
        en: '/about-us',
    },
    'contact/index': {
        sr: '/kontakt',
        en: '/contact',
    },
    'price-list/index': {
        sr: '/cenovnik',
        en: '/price-list',
    },
    'terms-of-service/index': {
        sr: '/uslovi-koriscenja',
        en: '/terms-of-service',
    },
    'job/details/_id': {
        sr: '/detalji-konkursa/:id',
        en: '/job-details/:id',
    },
    'job/search/_search_params': {
        sr: '/pretraga-konkursa/:search_params',
        en: '/job-search/:search_params',
    },
    'anti-disc/index': {
        sr: '/antidiskriminacioni-propisi',
        en: '/anti-discrimination-rules',
    },
    'privacy-policy/index': {
        sr: '/politika-privatnosti',
        en: '/privacy-policy',
    },
    'reset-password/index': {
        sr: '/promena-lozinke',
        en: '/password-reset',
    }
};