export default {
    'employer/index': {
        sr: '/poslodavci',
        en: '/employers',
    },
    'employer/login/index': {
        sr: '/poslodavac/prijava',
        en: '/employer/login',
    },
    'employer/register/index': {
        sr: '/poslodavac/registracija',
        en: '/employer/register',
    },
    'employer/dashboard/index': {
        sr: '/poslodavac/kontrolna-tabla',
        en: '/employer/dashboard',
    },
    'employer/profile/index': {
        sr: '/poslodavac/profil',
        en: '/employer/profile',
    },
    'employer/jobs/pending/index': {
        sr: '/poslodavac/moji-oglasi-na-cekanju',
        en: '/employer/my-pending-jobs',
    },
    'employer/jobs/index': {
        sr: '/poslodavac/oglasi',
        en: '/employer/jobs',
    },
    'employer/jobs/expired/index': {
        sr: '/poslodavac/moji-istekli-oglasi',
        en: '/employer/my-expired-jobs',
    },
    'employer/job-details/_id': {
        sr: '/poslodavac/detalji-posla/:id',
        en: '/candidate/job-apply/:id',
    },
    'employer/job-applications/_job_id': {
        sr: '/poslodavac/prijave-na-konkurs/:job_id',
        en: '/poslodavac/job-applications/:job_id',
    },
    'employer/new-job': {
        sr: '/poslodavac/kreiraj-oglas',
        en: '/poslodavac/craete-job',
    },
};