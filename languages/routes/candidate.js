export default {
    'candidate/login/index': {
        sr: '/kandidat/prijava',
        en: '/candidate/login',
    },
    'candidate/register/index': {
        sr: '/kandidat/registracija',
        en: '/candidate/register',
    },
    'candidate/profile/index': {
        sr: '/kandidat/moj-nalog',
        en: '/candidate/my-profile',
    },
    'candidate/documents/index': {
        sr: '/kandidat/moji-dokumenti',
        en: '/candidate/my-documents',
    },
    'candidate/job-apply/_id': {
        sr: '/kandidat/prijava-na-konkurs/:id',
        en: '/candidate/job-apply/:id',
    },
    'candidate/applications/index': {
        sr: '/kandidat/moje-prijave',
        en: '/candidate/my-job-applications',
    },
    'candidate/searches/index': {
        sr: '/kandidat/moje-pretrage',
        en: '/candidate/my-searches',
    },
    'candidate/saved-jobs/index': {
        sr: '/kandidat/sacuvani-oglasi',
        en: '/candidate/saved-jobs',
    },
};