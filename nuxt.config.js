import colors from 'vuetify/es5/util/colors'
import {I18n} from './config/I18n.js'


export default {
    // Global page headers: https://go.nuxtjs.dev/config-head
    target: "server",
    loading: '~/components/loading.vue',
    server: {
        port: process.env.NUXT_PORT || 3000,
        host: process.env.NUXT_HOST || 'localhost'
    },
    head: {
        titleTemplate: '%s - Lako do posla',
        title: 'Lako do posla',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: 'Želite posao? Na pravom ste mestu. Pretražite aktuelnu ponudu poslova.'},
            {name: 'format-detection', content: 'telephone=no'},
            {
                hid:'og:title',
                name:'og:title',
                content:'Lako do posla',
            },
            {
                hid:'og:image',
                name:'og:image',
                content:'https://www.lakodoposla.com/ldp-logo.png',
            },
        ],
        script: [
            {
                src: "https://www.google.com/recaptcha/api.js",
                async: true,
                defer: true
            }
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.png'},
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Roboto:ital@1&display=swap' }
        ],
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
        '~/assets/css/main.scss'
    ],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
        '~/plugins/core/persistantState/candidate-saved-jobs',
        '~/plugins/core/persistantState/candidate-auth',
        '~/plugins/core/persistantState/candidate-profile',
        '~/plugins/core/persistantState/common',
        '~/plugins/core/persistantState/employer-auth',
        '~/plugins/core/persistantState/employer-job-publisher',
        '~/plugins/core/persistantState/employer-profile',
        '~/plugins/core/persistantState/candidate-searches',

        '~/plugins/core/axios',
        '~/plugins/core/helper',
        '~/plugins/third_party/lodash.js',
        '~/plugins/filters/ucfirst.js',
        '~/plugins/filters/date.js',
        '~/plugins/filters/clear-route-string.js',
        '~/plugins/filters/filter_regions.js',
        '~/plugins/filters/html_decode.js',

    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,
    router: { middleware: 'scroll'},
    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [
        // https://go.nuxtjs.dev/vuetify
        '@nuxtjs/vuetify',
        '@nuxtjs/composition-api/module',
        '@nuxtjs/dotenv',
        '@nuxtjs/google-analytics'
    ],

    googleAnalytics: {
        id: 'G-BFNRZTG0TC'
      },

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        // https://go.nuxtjs.dev/pwa
        '@nuxtjs/pwa',
        // https://i18n.nuxtjs.org/
        'nuxt-i18n',
        //google tag manager
        '@nuxtjs/gtm'
    ],
    gtm: {
        id: 'G-BFNRZTG0TC'
    },
    // Axios module configuration: https://go.nuxtjs.dev/config-axios
    axios: {
        baseURL: process.env.AXIOS_BASE_URL || 'https://lakodoposla.com/'
        // baseURL: 'https://lakodoposla.com/'
        // baseURL: 'https://lkd.rs/'
        // baseURL: 'http://68.183.78.126/'
    },

    env: {
        baseUrl: process.env.BASE_URL || 'http://localhost:3000'
    },
    // PWA module configuration: https://go.nuxtjs.dev/pwa
    pwa: {
        manifest: {
            lang: 'sr'
        }
    },

    // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
    vuetify: {
        customVariables: ['~/assets/variables.scss'],
        treeShake: true,
        theme: {
            dark: false,
            themes: {
                dark: {
                    primary: colors.blue.darken2,
                    accent: colors.grey.darken3,
                    secondary: colors.amber.darken3,
                    info: colors.teal.lighten1,
                    warning: colors.amber.base,
                    error: colors.deepOrange.accent4,
                    success: colors.green.accent3
                },
                light: {
                    primary: colors.blue.darken2,
                    accent: colors.grey.darken3,
                    secondary: colors.amber.darken3,
                    info: colors.teal.lighten1,
                    warning: colors.amber.base,
                    error: colors.deepOrange.accent4,
                    success: colors.green.accent3
                }
            }
        }
    },

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {
        transpile: ['vuetify/lib', "tiptap-vuetify"]
    },

    generate: {
        // choose to suit your project
        interval: 2000,
    },

    // nuxt-i18n module options
    i18n: I18n
}
