export const JOB_PORMO_IMAGE_MUTATIONS = {
    SET_PROMO_IMAGES: 'SET_PROMO_IMAGES',
    SET_CURRENT_IMAGE: 'SET_CURRENT_IMAGE',
    RESET_ALL: 'RESET_ALL'
};

export default {
    [JOB_PORMO_IMAGE_MUTATIONS.SET_PROMO_IMAGES] (state, images) {
        state.list = images;
    },

    [JOB_PORMO_IMAGE_MUTATIONS.SET_CURRENT_IMAGE] (state, image) {
        state.current_image = image
    },

    [JOB_PORMO_IMAGE_MUTATIONS.RESET_ALL] (state) {
        state.list = [];
        state.current_image = '';
    },
}