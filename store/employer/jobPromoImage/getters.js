export default {
    getFirstPromoImage (state) {
        return state.list.length ? state.list[0] : ''
    },

    getCurrentPromoImage (state) {
        return state.current_image
    }
}