import {JOB_PORMO_IMAGE_MUTATIONS} from "./mutations";
import {DOCUMENTS_MUTATIONS} from "../../candidate/documents/mutations";
import {PROFILE_MUTATIONS} from "../profile/mutations";

export default {
    setPromoImages({commit, dispatch}, categories) {
        return new Promise(resolve => {
            if (!categories.length) {
                commit(JOB_PORMO_IMAGE_MUTATIONS.SET_CURRENT_IMAGE, '')
                commit(JOB_PORMO_IMAGE_MUTATIONS.SET_PROMO_IMAGES, [])
                return resolve([])
            }
            let cats = categories.join(',');
            this.$axios.get('api/employer/get_category_featured_images?cat=' + cats)
                .then(response => {
                    let images = [];
                    let catIds = Object.getOwnPropertyNames(response.data.category);

                    for (let i in catIds) {
                        images = [...images, ...response.data.category[catIds[i]]]
                    }

                    images = [...images, ...response.data.employer]

                    commit(JOB_PORMO_IMAGE_MUTATIONS.SET_PROMO_IMAGES, images)
                    commit(JOB_PORMO_IMAGE_MUTATIONS.SET_CURRENT_IMAGE, images.length ? images[0] : '')

                    resolve(images);
                }).catch(error => {

            })
        })
    },

    setNextPromoImage({state, commit}) {
        let promoImageIndex = _.findIndex(state.list, function (image) {
            return image == state.current_image
        })
        let lengthOfArray = state.list.length - 1;

        console.log('lengthOfArray', lengthOfArray)
        console.log('promoImageIndex', promoImageIndex)
        if (promoImageIndex == lengthOfArray) {
            commit(JOB_PORMO_IMAGE_MUTATIONS.SET_CURRENT_IMAGE, state.list[0])
        } else {
            commit(JOB_PORMO_IMAGE_MUTATIONS.SET_CURRENT_IMAGE, state.list[promoImageIndex + 1])
        }
    },

    setPrevPromoImage({state, commit}) {
        let promoImageIndex = _.findIndex(state.list, function (image) {
            return image == state.current_image
        })
        let lengthOfArray = state.list.length - 1;

        if (promoImageIndex == 0) {
            commit(JOB_PORMO_IMAGE_MUTATIONS.SET_CURRENT_IMAGE, state.list[lengthOfArray])
            return state.list[lengthOfArray]
        } else {
            commit(JOB_PORMO_IMAGE_MUTATIONS.SET_CURRENT_IMAGE, state.list[promoImageIndex - 1])
        }
    },

    uploadNewImage({commit, dispatch, state, rootState}, file) {
        return new Promise(resolve => {

            let fd = new FormData();
            fd.append('featured_image', file);

            this.$axios.post('/api/employer/upload_featured_image', fd, {
                headers: {
                    'Content-Type': 'multipart/formdata'
                }
            })
                .then(response => {
                    let uploadedImage = response.data.feature_image_url;
                    let newImageArray = [];

                    state.list.map(image => {
                        if (image == state.current_image) {
                            newImageArray.push(image);
                            newImageArray.push(uploadedImage)
                        } else {
                            newImageArray.push(image);
                        }
                    })

                    commit(JOB_PORMO_IMAGE_MUTATIONS.SET_PROMO_IMAGES, newImageArray)
                    commit(JOB_PORMO_IMAGE_MUTATIONS.SET_CURRENT_IMAGE, uploadedImage)

                    return resolve(uploadedImage)
                }).catch(error => {
                commit(JOB_PORMO_IMAGE_MUTATIONS.SET_CURRENT_IMAGE, '')
                return resolve('')
            })
        })

    },


    setCurrentPromoImage({commit}, image) {
        commit(JOB_PORMO_IMAGE_MUTATIONS.SET_CURRENT_IMAGE, image)
    },

    resetAll({commit}) {
        commit(JOB_PORMO_IMAGE_MUTATIONS.RESET_ALL)
    }

}
