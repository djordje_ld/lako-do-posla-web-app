import {JOB_PUBLISHER_MUTATIONS} from "./mutations";

export default {
    async set ({ commit, dispatch }, {field, value}) {
        commit(JOB_PUBLISHER_MUTATIONS.SET, {field, value})
    },
}
