// reusable aliases for mutations
export const JOB_PUBLISHER_MUTATIONS = {
    SET: 'SET',
};

export default {
    [JOB_PUBLISHER_MUTATIONS.SET] (state, {field, value}) {
        state[field] = value;
    }
}