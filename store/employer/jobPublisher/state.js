export default () => ({
    name: '',
    email: '',
    phone: '',
    is_director: '',
    director_name: '',
    director_email: '',
    director_phone: '',
})