import {NEW_JOB_MUTATIONS} from "./mutations";
import {DOCUMENTS_MUTATIONS} from "../../candidate/documents/mutations";

export default {
    async set ({ commit, dispatch }, {fieldName, value}) {
        commit(NEW_JOB_MUTATIONS.SET, {fieldName, value})
    },

    async setDefault ({ commit }) {
        commit(NEW_JOB_MUTATIONS.SET_DEFAULT)
    },

    async setQuestionIndex ({ commit, dispatch }, questionIndex) {
        commit(NEW_JOB_MUTATIONS.SET_QUESTION_INDEX, questionIndex)
    },

    async resetMessage ({ commit }) {
        commit(NEW_JOB_MUTATIONS.RESET_MESSAGE)
    },

    async publishJob ({ commit, dispatch }, dataToPost) {
        dispatch('overlayLoader/toggleOverlayLoader', true, {root:true});

        // hack
        dataToPost.region = dataToPost.regions;
        // ---


        this.$axios.post(
            '/api/employer/posts',
            JSON.stringify(dataToPost),
            {
              headers:{
                "Content-Type" : "application/json",
              }
            }
        ) .then(async result => {
            await  dispatch('attachFileToJobPost', result.data.id)

            dispatch('overlayLoader/toggleOverlayLoader', false, {root:true});
            dispatch('mainDialog/jobPosted',[true, ''], {root:true});
            dispatch('common/setJobJustCreated', true, {root:true});

            this.$router.push(this.localePath({ name: 'employer-jobs', query: {type: 'pending'} }))
        }) .catch(error => {
            dispatch('overlayLoader/toggleOverlayLoader', false, {root:true});
            let errors = '';
            error.response.data.error.map(error => errors += (error + '<br>'))
            dispatch('mainDialog/jobPosted',[false, errors], {root:true});
        })
    },

    async recreateJob ({ commit, dispatch, rootGetters}, job) {
        this.$axios.get('api/public/posts/'+job.id)
            .then(async response => {
                let data = {...job, ...response.data}
                data.categories = data.categories.map(category => Number(category))
                data.regions = data.region.map(region => Number(region))
                data.contract_type_id = Number(data.contract_type_id)
                data.career_levels = data.career_levels.map(career_level => Number(career_level))

                dispatch('employer/questionnaire/setQuestionnaireFromOldJob', job.questionnaire, {root:true})
                commit(NEW_JOB_MUTATIONS.RECREATE_JOB, data)

                await dispatch('employer/jobPromoImage/setPromoImages', data.categories, {root:true});
                dispatch('setPromoImage', data.promo_image ? data.promo_image : rootGetters['employer/jobPromoImage/getFirstPromoImage'])

                this.$router.push(this.localePath('employer-new-job'))
            })
    },

    async setProductCode ({ commit, rootState}, productCode) {
        commit(NEW_JOB_MUTATIONS.SET_PRODUCT_CODE, productCode)
    },

    setFile ({ commit }, file) {
        commit(NEW_JOB_MUTATIONS.SET_FILE, file)
    },

    setOpenPdfDirectly ({ commit }, status) {
        commit(NEW_JOB_MUTATIONS.SET_OPEN_PDF_DIRECTRLY, status)
    },

    attachFileToJobPost ({ state, commit, dispatch}, jobId) {
        return new Promise(resolve => {
            if (state.file.upload_document) {
                let fd = state.file.upload_document;
                fd.append('posting_id', jobId);
                fd.append('open_pdf_directly', state.file.upload_document ? 1 : 0);

                this.$axios.post('/api/employer/add_posting_document', fd, {
                    headers: {
                        'Content-Type': 'multipart/formdata'
                    }
                })
                    .then(() => {
                        commit('setFile', null)
                        commit('setOpenPdfDirectly', null)
                        resolve()
                    }) .catch(() => {
                    commit('setFile', null)
                    commit('setOpenPdfDirectly', null)
                    resolve()
                })
            }
            else {
                resolve();
            }
        })
    },


    setPromoImage ({ commit, dispatch}, image) {
        commit(NEW_JOB_MUTATIONS.SET_PROMO_IMAGE, image)
    }
}
