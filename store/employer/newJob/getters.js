export default {
    enableSecondPart (state) {
        return !!state.product_code
    },
    enableThirdPart (state) {
        return (
            !!state.product_code
            && !!state.title.length
            && !!state.job_categories.length
            && !!state.regions.length
            && !!state.language_id
            && !!state.contract_type_id
            && !!state.career_levels.length
        );
    },
    enableFourthPart (state) {
        return (
            !!state.product_code
            && !!state.title.length
            && !!state.job_categories.length
            && !!state.regions.length
            && !!state.language_id
            && !!state.contract_type_id
            && !!state.career_levels.length
            && !!state.expires
        );
    },
    enableFifthPart (state) {
        return (
            !!state.product_code
            && !!state.title.length
            && !!state.job_categories.length
            && !!state.regions.length
            && !!state.language_id
            && !!state.contract_type_id
            && !!state.career_levels.length
            && !!state.expires
            && !!state.to_email
        );
    },
    enableSixthPart (state) {
        return !!state.message
    },

    getPromoImage (state) {
        return state.promo_image
    }
}