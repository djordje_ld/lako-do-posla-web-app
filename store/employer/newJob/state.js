export const defaultNewJobState = {
    product_code:'',
    // type_title:'',
    // type_days:'',
    title:'',
    job_categories:[],
    regions:[],
    message:'',
    language_id:'',
    contract_type_id:'',
    career_levels:[],
    min_education:0,
    max_education:0,
    expires:'',
    to_email:'',
    promo_image: '',
    questionnaire: {
        //this will be populated at the end
    },
    file: {
        upload_document: null,
        open_pdf_directly: false
    }
}
export default () => (defaultNewJobState)
