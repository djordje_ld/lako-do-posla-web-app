import {defaultNewJobState} from "@/store/employer/newJob/state";

export const NEW_JOB_MUTATIONS = {
    SET: 'SET',
    SET_DEFAULT: 'SET_DEFAULT',
    RECREATE_JOB: 'RECREATE_JOB',
    SET_PRODUCT_CODE: 'SET_PRODUCT_CODE',
    RESET_MESSAGE: 'RESET_MESSAGE',
    SET_FILE: 'SET_FILE',
    SET_OPEN_PDF_DIRECTRLY: 'SET_OPEN_PDF_DIRECTRLY',
    SET_PROMO_IMAGE: 'SET_PROMO_IMAGE',
};

export default {
    [NEW_JOB_MUTATIONS.SET] (state, {fieldName, value}) {
        state[fieldName] = value;
    },

    [NEW_JOB_MUTATIONS.SET_DEFAULT] (state) {
        Object.keys(defaultNewJobState).map(key => {
            state[key] = defaultNewJobState[key]
        })
    },

    [NEW_JOB_MUTATIONS.RECREATE_JOB] (state, job) {
        Object.keys(state).map(key => {
            switch(key) {
                case 'job_categories':
                    state[key] = job.categories;
                    break;
                case 'max_education':
                    state[key] = Number(job.education_levels[job.education_levels.length-1]);
                    break;
                case 'min_education':
                    state[key] = Number(job.education_levels[0]);
                    break;
                case 'message':
                    state[key] = this.$helper.decodeHTML(job.message);
                    break;
                case 'regions':
                    state[key] = job.regions.map(region => Number(region))
                    break;
                case 'expires':
                    state[key] = ''
                    break;
                case 'file':
                    state[key] = state[key]
                    break;
                default:
                    state[key] = job[key]
            }
        })
    },

    [NEW_JOB_MUTATIONS.SET_PRODUCT_CODE] (state, productCode) {
        state.product_code = productCode
    },

    [NEW_JOB_MUTATIONS.RESET_MESSAGE] (state) {
        state.message = '';
    },

    [NEW_JOB_MUTATIONS.SET_FILE] (state, file) {
        state.file.upload_document = file;
    },

    [NEW_JOB_MUTATIONS.SET_OPEN_PDF_DIRECTRLY] (state, status) {
        state.file.open_pdf_directly = status;
    },

    [NEW_JOB_MUTATIONS.SET_PROMO_IMAGE] (state, image) {
        state.promo_image = image
    },


}