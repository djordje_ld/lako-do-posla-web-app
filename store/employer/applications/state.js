import {initialExpiredJobsPagination} from "./mutations";

export default () => ({
    all: [],
    filtered: [],
    questions: [],
    questionnaire: null,
    filterByQuestionnaire: [],
    sort: {
        by:'date',
        asc_desc: 'asc'
    },
    filters: {
        grade: [5],
        questionnaire: []
    },
    mobileApplicantDetails: false,
    jobForApplication: null
})