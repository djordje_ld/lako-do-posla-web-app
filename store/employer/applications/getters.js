export default {
  getAll(state, context) {
    return state.all
  },

  getFiltered(state, context) {
    return state.filtered
  },

  getSort(state) {
    return state.sort
  },

  getQuestionnaireForCurrentJob(state) {
    return state.questionnaire
  }
}