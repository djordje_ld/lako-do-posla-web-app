// reusable aliases for mutations
export const APPLICATIONS_MUTATIONS = {
    SET_ALL_APP_DATA: 'SET_ALL_APP_DATA',
    SET_FILTERED: 'SET_FILTERED',
    SORT: 'SORT',
    SET_GRADE: 'SET_GRADE',
    SET_GRADE_FILTER: 'SET_GRADE_FILTER',
    SET_QUESTIONNAIRE_FILTER: 'SET_QUESTIONNAIRE_FILTER',
    SET_GRADE_LOADER: 'SET_GRADE_LOADER',
    SET_QUESTIONNAIRE_FOR_CURRENT_JOB: 'SET_QUESTIONNAIRE_FOR_CURRENT_JOB',
    UPDATE_COMMENT: 'UPDATE_COMMENT',
    SET_MOBILE_APPLICANT_DETAILS: 'SET_MOBILE_APPLICANT_DETAILS',
    SET_JOB_FOR_APPLICATION: 'SET_JOB_FOR_APPLICATION',
};
export default {
    [APPLICATIONS_MUTATIONS.SET_ALL_APP_DATA] (state, {apps, questions}) {
        for(let i in apps) {
            apps[i].satisfactoryLoader = false
            apps[i].unsatisfactoryLoader = false
            apps[i].unassignedLoader = false
        }
        state.all = apps;
        state.filtered = apps;
        state.questions = questions;
    },

    [APPLICATIONS_MUTATIONS.SET_QUESTIONNAIRE_FOR_CURRENT_JOB] (state, questionnaire) {
        state.questionnaire = questionnaire
    },

    [APPLICATIONS_MUTATIONS.SET_MOBILE_APPLICANT_DETAILS] (state, status) {
        state.mobileApplicantDetails = status
    },

    [APPLICATIONS_MUTATIONS.SET_FILTERED] (state, filteredApps) {
        state.filtered = filteredApps
    },

    [APPLICATIONS_MUTATIONS.SORT] (state, sortOption) {
        if (sortOption) {
            state.sort.by = sortOption.by
            state.sort.asc_desc = sortOption.asc_desc
        }
    },

    [APPLICATIONS_MUTATIONS.UPDATE_COMMENT] (state, {appId, comment}) {
            state.all.map((app,index) => {
                if (app.application_id == appId) {

                    app.comment = comment;
                }
            })
            state.filtered.map(app => {
                if (app.application_id == appId) {
                    app.comment = comment;
                }
            })
    },
    [APPLICATIONS_MUTATIONS.SET_GRADE] (state, {appId, grade}) {
        state.all.map(app => app.grade = (app.application_id == appId) ? grade : app.grade)
        state.filtered.map(app => app.grade = (app.application_id == appId) ? grade : app.grade)
    },

    [APPLICATIONS_MUTATIONS.SET_GRADE_FILTER] (state, grade) {
        state.filters.grade = grade;
    },

    [APPLICATIONS_MUTATIONS.SET_QUESTIONNAIRE_FILTER] (state, qFilters) {
        state.filters.questionnaire = qFilters;
    },

    [APPLICATIONS_MUTATIONS.SET_GRADE_LOADER] (state, {appId, loaderName, status}) {
        for(let i in state.all) {
            if (state.all[i].application_id == appId) {
                state.all[i][loaderName] = status;
                state.filtered[i][loaderName] = status;
                return;
            }
        }
    },

    [APPLICATIONS_MUTATIONS.SET_JOB_FOR_APPLICATION] (state, job) {
        state.jobForApplication = job;
    },
}