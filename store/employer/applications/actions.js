import {APPLICATIONS_MUTATIONS} from "./mutations";

export default {
    async setAllAppData ({ commit, dispatch }, jobId) {
        const {data: apps} = await this.$axios.get('/api/employer/applications/'+jobId);
        const {data: questions} = await this.$axios.get('/api/public/questions/'+jobId);
        commit(APPLICATIONS_MUTATIONS.SET_ALL_APP_DATA, {apps, questions})
    },

    sort ({state, commit}, sortOptions = null) {
        let asc_desc = sortOptions ? sortOptions.asc_desc : state.asc_desc;
        let by = sortOptions ? sortOptions.by : state.by;
        
        let sorted = (asc_desc === 'asc') ?
            this._vm.$_.sortBy(state.filtered, by) :
            this._vm.$_.sortBy(state.filtered, by).reverse();

        commit(APPLICATIONS_MUTATIONS.SET_FILTERED, sorted)
        commit(APPLICATIONS_MUTATIONS.SORT, sortOptions)
    },
    async setQuestionnaireForCurrentJob ({state, commit}, jobId = null) {
        if (!jobId) return commit(APPLICATIONS_MUTATIONS.SET_QUESTIONNAIRE_FOR_CURRENT_JOB, null)

        const {data: questionnaire} = await this.$axios.get('/api/public/questions/'+jobId);
        if (questionnaire.length) {
            commit(APPLICATIONS_MUTATIONS.SET_QUESTIONNAIRE_FOR_CURRENT_JOB, questionnaire)
        } else {
            commit(APPLICATIONS_MUTATIONS.SET_QUESTIONNAIRE_FOR_CURRENT_JOB, null)
        }
    },

    async updateComment ({state, commit}, {appId, comment}) {
        commit(APPLICATIONS_MUTATIONS.UPDATE_COMMENT, {appId, comment})
    },

    setMobileApplicantDetails ({state, commit}, status) {
        commit(APPLICATIONS_MUTATIONS.SET_MOBILE_APPLICANT_DETAILS, status)
    },

    filterByGrade ({state, commit, dispatch}, grades) {
        commit(APPLICATIONS_MUTATIONS.SET_GRADE_FILTER, grades)
        dispatch('sort')
        dispatch('filter')
    },

    filter ({state, commit, dispatch}) {
        // filter by grade
        if (state.filters.grade.length == 1 && state.filters.grade[0] == 5) {
            commit(APPLICATIONS_MUTATIONS.SET_FILTERED, state.all)
        } else {
            let filtered = state.all.filter(app => {
                return state.filters.grade.indexOf(Number(app.grade)) > -1
            })
            commit(APPLICATIONS_MUTATIONS.SET_FILTERED, filtered)
        }

        if (!state.filters.questionnaire.length) {
            return commit(APPLICATIONS_MUTATIONS.SET_FILTERED, state.filtered)
        }
        
        let satisfying = [];
        state.filtered.map(app => {
            for (let i in state.filters.questionnaire) {
                let fq = state.filters.questionnaire[i];
                let appAnswer = app.answers[fq.ansId];

                //if there is no answer by this questions filter (this probably will never be the case)
                if (!appAnswer) {
                    return;
                }

                //if is question type was radio OR logic will be applied
                if (!Array.isArray(appAnswer)) {
                    if (fq.optIds.indexOf(appAnswer) === -1) {
                        return;
                    } else {
                        continue;
                    }
                }

                //if is question type was checkbox AND logic will be applied
                if (Array.isArray(appAnswer)) {
                    for (let y in fq.optIds) {
                        if (appAnswer.indexOf(fq.optIds[y]) === -1) {
                            return;
                        }
                    }
                }
            }
            satisfying.push(app);
        })
        
        return commit(APPLICATIONS_MUTATIONS.SET_FILTERED, satisfying)
    },

    async setFBQ ({ commit, dispatch, state }, {answerId, optionId, checked}) {
        let string = JSON.stringify(state.filters.questionnaire)
        let parsed = JSON.parse(string);
        let checkedAnswers = [...parsed]
        if (checked) {
            let index = _.findIndex(checkedAnswers, {ansId: answerId})
            if (index === -1) {
                checkedAnswers.push({ansId: answerId, optIds: [optionId]})
            } else {
                checkedAnswers[index].optIds.push(optionId);
            }
        } else {
            let index = _.findIndex(checkedAnswers, {ansId: answerId})
            checkedAnswers[index].optIds = checkedAnswers[index].optIds.filter(optId => optionId != optId)
            if (!checkedAnswers[index].optIds.length) {
                checkedAnswers.splice(index, 1)
                checkedAnswers.sort()
            }
        }

        commit(APPLICATIONS_MUTATIONS.SET_QUESTIONNAIRE_FILTER, checkedAnswers)
        dispatch('filter')
    },



    setGrade ({ commit, dispatch }, {appId, grade}) {
        return new Promise(async (resolve, reject) => {
            let {data:data} = await this.$axios.patch('api/employer/application_grade/'+appId+'/'+grade)
            commit(APPLICATIONS_MUTATIONS.SET_GRADE, {appId, grade})
            resolve(data)
        })
    },

    async setGradeLoader ({ commit, dispatch }, {appId, loaderName, status}) {
        commit(APPLICATIONS_MUTATIONS.SET_GRADE_LOADER, {appId, loaderName, status})
    },

    setJobForApplication ({ commit, dispatch }, job) {
        commit(APPLICATIONS_MUTATIONS.SET_JOB_FOR_APPLICATION, job)
    }

}
