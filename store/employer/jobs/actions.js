import {JOBS_MUTATIONS} from "./mutations";
import {JOB_PORMO_IMAGE_MUTATIONS} from "../jobPromoImage/mutations";

export default {
    setExpNextPage (context) {
        context.commit(JOBS_MUTATIONS.EXP_SET_NEXT_PAGE);
    },

    setExpPrevPage (context) {
        context.commit(JOBS_MUTATIONS.EXP_SET_PREV_PAGE);
    },

    unsetJobs (context) {
        context.commit(JOBS_MUTATIONS.UNSET_JOBS);
    },

    searchExpired (context, searchTerm) {
        context.commit(JOBS_MUTATIONS.APPLY_SEARCH, searchTerm);
    },

    async setJobs (context) {
        let jobPosts = {
            all: [],
            active: [],
            expired: [],
        };
        let {data: all} = await this.$axios.get('/api/employer/posts');
        let {data: active} = await this.$axios.get('/api/employer/posts/active');
        let {data: expired} = await this.$axios.get('/api/employer/posts/expired');

        context.commit(JOBS_MUTATIONS.SET_JOBS, { all, active, expired });

        // jobPosts.all = j;
        // let {data: data} = await this.$axios.get('/api/employer/posts/');
        // jobPosts.acive = data.filter(job => {
           return
        // });
        let token = await vm.checkTokenStatus(true);
        return new Promise((resolve, reject) => {
            let jobPosts = [];
            vm.$http.get(vm.api_host + '/api/employer/posts', {headers: {"Authorization": "Bearer " + token}})
                .then(response => {
                    jobPosts.all = response.body;
                    vm.$http.get(vm.api_host + '/api/employer/posts/active', {headers: {"Authorization": "Bearer " + token}})
                        .then(response => {
                            jobPosts.active = response.body;
                            vm.$http.get(vm.api_host + '/api/employer/posts/expired', {headers: {"Authorization": "Bearer " + token}})
                                .then(response => {
                                    jobPosts.expired = response.body;
                                    handlers.employer.setJobPosts(jobPosts); // sets jobPosts in localStorage
                                    context.commit('setEmployersJobPosts', jobPosts);
                                    resolve(true)
                                })
                                .catch(error => {
                                    resolve(error)
                                })
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
                .catch(error => {
                    reject(error)
                })
        }).catch(er => {
            return er;
        });
    },
    updatePromoImage({commit, dispatch, state, rootState}, jobId) {
        return new Promise(resolve => {

            let jsonObject = JSON.stringify({
                promo_image:rootState.employer.jobPromoImage.current_image,
            })

            this.$axios.patch('api/employer/update_posting_promo_image/'+jobId, jsonObject,{
                headers: {
                    'Content-Type' : 'application/json'
                }
            })
            .then(response => {
                commit(JOBS_MUTATIONS.SET_JOB_PROMO_IMAGE, {promoImage:rootState.employer.jobPromoImage.current_image, jobId});
                return resolve(true)
                }).catch(error => {
                return resolve(false)
            })
        })

    },
}
