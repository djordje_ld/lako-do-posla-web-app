export default {
  getActiveJobs(state, context) {
    return state.active
  },
  getExpiredJobs(state, context) {
    return state.expired
  },
  getExpiredPaginated(state) {
    let form = (state.expiredJobsPagination.current === 1) ? 0 : state.expiredJobsPagination.per_page * (state.expiredJobsPagination.current - 1);
    let to = (state.expiredJobsPagination.current === 1) ? state.expiredJobsPagination.per_page : (state.expiredJobsPagination.current * state.expiredJobsPagination.per_page)
    return state.expiredFiltered.slice(form, to);
  },

  getExpiredAllowNext(state) {
      return state.expiredJobsPagination.allow_next
  },

  getExpiredAllowPrev(state) {
    return state.expiredJobsPagination.allow_prev
  },

  getTotalPages(state) {
    return Math.round(state.expiredJobsPagination.total / state.expiredJobsPagination.per_page)
  },

  getCurrent(state) {
    return state.expiredJobsPagination.current
  },
}