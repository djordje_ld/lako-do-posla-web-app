// reusable aliases for mutations
export const JOBS_MUTATIONS = {
    EXP_SET_NEXT_PAGE: 'EXP_SET_NEXT_PAGE',
    EXP_SET_PREV_PAGE: 'EXP_SET_PREV_PAGE',
    EXP_RESET_PAGINATION: 'EXP_RESET_PAGINATION',
    SET_JOBS: 'EXP_SET_JOBSRESET_PAGINATION',
    UNSET_JOBS: 'UNSET_JOBS',
    APPLY_SEARCH: 'APPLY_SEARCH',
    SET_JOB_PROMO_IMAGE: 'SET_JOB_PROMO_IMAGE',
};

export const initialExpiredJobsPagination = {
    current: 1,
    previous: 0,
    per_page: 5,
    total: 0,
    allow_prev: false,
    allow_next: true,
}

export default {
    [JOBS_MUTATIONS.EXP_SET_NEXT_PAGE] (state) {
        state.expiredJobsPagination.current = state.expiredJobsPagination.current + 1;
        state.expiredJobsPagination.allow_prev = true;
        state.expiredJobsPagination.allow_next = (state.expiredJobsPagination.current * state.expiredJobsPagination.per_page < state.expiredJobsPagination.total);
    },

    [JOBS_MUTATIONS.EXP_SET_PREV_PAGE](state) {
        state.expiredJobsPagination.current = state.expiredJobsPagination.current - 1;
        state.expiredJobsPagination.allow_prev = (state.expiredJobsPagination.current > 1)
        state.expiredJobsPagination.allow_next = (state.expiredJobsPagination.current * state.expiredJobsPagination.per_page < state.expiredJobsPagination.total);
    },

    [JOBS_MUTATIONS.EXP_RESET_PAGINATION] (state) {
        state.expiredJobsPagination = initialExpiredJobsPagination;
    },

    [JOBS_MUTATIONS.SET_JOBS] (state, jobPosts) {

        state.all = jobPosts.all;
        state.active = jobPosts.active;
        state.expired = jobPosts.expired;
        state.expiredFiltered = jobPosts.expired;
        state.expiredJobsPagination = {
            current: 1,
            previous: 0,
            per_page: state.expiredJobsPagination.per_page,
            total: jobPosts.expired.length,
            allow_prev: false,
            allow_next: true,
        };
    },
    [JOBS_MUTATIONS.UNSET_JOBS] (state) {
        state.all = null;
        state.active = null;
        state.expired = null;
    },
    [JOBS_MUTATIONS.APPLY_SEARCH] (state, searchTerm) {
        if (!searchTerm) {
            state.expiredJobsPagination.total = state.expired.length
            return state.expiredFiltered = state.expired;
        }
        let filtered = [];
        state.expired.map(job => {
            const title = job.title;
            if (title.toLocaleLowerCase().indexOf(searchTerm) > -1) {
                filtered.push(job);
            }
        });
        state.expiredFiltered = filtered;
        state.expiredJobsPagination.total = state.expiredFiltered.length
    },
    [JOBS_MUTATIONS.SET_JOB_PROMO_IMAGE] (state, {promoImage, jobId}) {
        let allJobsJobIndex = _.findIndex(state.all, function (job) { return jobId == job.id })
        let activeJobsJobIndex = _.findIndex(state.all, function (job) { return jobId == job.id })
        let expiredJobsJobIndex = _.findIndex(state.all, function (job) { return jobId == job.id })
        let expiredFilteredJobsJobIndex = _.findIndex(state.all, function (job) { return jobId == job.id })

        if (allJobsJobIndex > -1) {
            state.all[allJobsJobIndex].promo_image = promoImage;
        }
        if (activeJobsJobIndex > -1) {
            state.active[activeJobsJobIndex].promo_image = promoImage;
        }
        if (expiredJobsJobIndex > -1) {
            state.expired[expiredJobsJobIndex].promo_image = promoImage;
        }
        if (expiredFilteredJobsJobIndex > -1) {
            state.expiredFiltered[expiredFilteredJobsJobIndex].promo_image = promoImage;
        }
    }
}