import {initialExpiredJobsPagination} from "./mutations";

export default () => ({
    expiredJobsPagination: {
        ...initialExpiredJobsPagination
    },
    all: [],
    active: [],
    expired: [],
    expiredFiltered: []
})