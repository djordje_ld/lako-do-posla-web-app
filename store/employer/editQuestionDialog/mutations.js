export const EDIT_QUESTION_MUTATIONS = {
    SET_STATUS: 'SET_STATUS',
    SET_QUESTION: 'SET_QUESTION',
    SET_QUESTION_INDEX: 'SET_QUESTION_INDEX',
    SET_QUESTION_TITLE: 'SET_QUESTION_TITLE',
    UPDATE_ANSWER_TYPE: 'UPDATE_ANSWER_TYPE',
    UPDATE_ANSWER: 'UPDATE_ANSWER',
    DELETE_ANSWER: 'DELETE_ANSWER',
    ADD_ANSWER: 'ADD_ANSWER',
}
export default {
    [EDIT_QUESTION_MUTATIONS.SET_STATUS] (state, payload) {
        if (payload) {
            state.status = payload;
        } else {
            state.status = payload;
            state.question = null;
        }

    },

    [EDIT_QUESTION_MUTATIONS.SET_QUESTION] (state, payload) {
       state.question = payload;
    },

    [EDIT_QUESTION_MUTATIONS.SET_QUESTION_INDEX] (state, questionIndex) {
        state.questionIndex = questionIndex;
    },

    [EDIT_QUESTION_MUTATIONS.SET_QUESTION_TITLE] (state, title) {
        state.question.title = title;
    },

    [EDIT_QUESTION_MUTATIONS.UPDATE_ANSWER_TYPE] (state, type) {
        if (type == 'text') {
            state.question.answers = [];
        }
        state.question.type = type;
    },

    [EDIT_QUESTION_MUTATIONS.UPDATE_ANSWER] (state, {title, answerIndex}) {
        state.question.answers[answerIndex].title = title;
    },

    [EDIT_QUESTION_MUTATIONS.DELETE_ANSWER] (state, answerIndex) {
        state.question.answers.splice(answerIndex, 1);
    },

    [EDIT_QUESTION_MUTATIONS.ADD_ANSWER] (state) {
        state.question.answers.push({
            satisfying : false,
            title : ""
        });
    }
}