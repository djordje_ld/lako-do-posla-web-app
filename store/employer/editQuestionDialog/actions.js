import {EDIT_QUESTION_MUTATIONS} from "./mutations";

export default {
    setStatus({commit}, payload) {
        commit(EDIT_QUESTION_MUTATIONS.SET_STATUS, payload)
    },

    setQuestion({commit}, payload) {
        commit(EDIT_QUESTION_MUTATIONS.SET_QUESTION, payload)
    },

    setQuestionIndex({commit}, questionIndex) {
        commit(EDIT_QUESTION_MUTATIONS.SET_QUESTION_INDEX, questionIndex)
    },

    setQuestionTitle({commit}, title) {
        commit(EDIT_QUESTION_MUTATIONS.SET_QUESTION_TITLE, title)
    },

    updateAnswerType({commit}, type) {
        commit(EDIT_QUESTION_MUTATIONS.UPDATE_ANSWER_TYPE, type)
    },

    updateAnswer({commit}, {title, answerIndex}) {
        commit(EDIT_QUESTION_MUTATIONS.UPDATE_ANSWER, {title, answerIndex})
    },

    addAnswer({commit}) {
        commit(EDIT_QUESTION_MUTATIONS.ADD_ANSWER)
    },

    deleteAnswer({commit}, answerIndex) {
        commit(EDIT_QUESTION_MUTATIONS.DELETE_ANSWER, answerIndex)
    },

}
