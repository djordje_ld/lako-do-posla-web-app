import {PROFILE_MUTATIONS} from "../../employer/profile/mutations";

export default {
    async setProfile ({ commit, dispatch }, data = null) {
        if (data) {
            commit(PROFILE_MUTATIONS.SET_PROFILE, data)
        } else {
            const {data: data} = await this.$axios.get('/api/employer/data')
            commit(PROFILE_MUTATIONS.SET_PROFILE, data)
        }

    },

    reset ({ commit, dispatch }) {
        commit(PROFILE_MUTATIONS.SET_PROFILE, {})
    },

    async update({commit, dispatch, state}, dataToUpdate) {

        await this.$axios.patch(
            'api/employer/data',
            JSON.stringify(dataToUpdate),
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(response => {
                commit(PROFILE_MUTATIONS.SET_PROFILE, response.data);
                dispatch('mainDialog/profileUpdated', true, {root: true})
            })
            .catch(error => {
                dispatch('mainDialog/profileUpdated', false, {root: true})
            })
    },

    async setProfileImage({commit}, file) {
        let formData = new FormData();
        formData.append('logo', file, file.name);
        return new Promise(resolve => {
            this.$axios.post(
                '/api/employer/logo',
                formData, {
                    headers: {
                        'Content-Type': 'multipart/formdata'
                    }
                })
                .then(async response => {
                    resolve(
                        commit(PROFILE_MUTATIONS.SET_PROFILE_IMAGE, {
                            hot_logo_path: response.data.hot_logo_path,
                            logo_path: response.data.logo_path,
                            logo: response.data.logo_path,
                        })
                    );
                }).then(error => resolve(error))
        })

    }

}
