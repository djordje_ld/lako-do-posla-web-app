// reusable aliases for mutations
export const PROFILE_MUTATIONS = {
    SET_PROFILE: 'SET_PROFILE',
    SET_PROFILE_IMAGE: 'SET_PROFILE_IMAGE'
};

export default {
    [PROFILE_MUTATIONS.SET_PROFILE] (state, data) {
        state.data = {...data};
    },

    [PROFILE_MUTATIONS.SET_PROFILE_IMAGE] (state, {hot_logo_path, logo_path}) {
        state.data.hot_logo = hot_logo_path;
        state.data.logo = logo_path;
    }
}