// reusable aliases for mutations
export const AUTH_MUTATIONS = {
    SET_USER: 'SET_USER',
    SET_TOKEN: 'SET_TOKEN',
    LOGOUT: 'LOGOUT',
}
export default {
    [AUTH_MUTATIONS.SET_USER] (state, { id, email_address }) {
        state.id = id
        state.email_address = email_address
    },
    [AUTH_MUTATIONS.SET_TOKEN] (state, {token, refresh_token, expires}) {
        state.token = token;
        state.refresh_token = refresh_token;
        state.expires = expires;
    },
    [AUTH_MUTATIONS.LOGOUT] (state) {
        state.token = null;
        state.refresh_token = null;
        state.expires = null;
    },
}