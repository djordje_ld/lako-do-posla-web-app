import {AUTH_MUTATIONS} from "./mutations";

export default {
    async login ({ commit, dispatch,state, rootState }, { email, password }) {
        try {
            dispatch('overlayLoader/toggleOverlayLoader', true, {root:true});

            const {data: data} = await this.$axios.post('/api/employer_token', {email, password})
            dispatch('common/setUserType', 'employer', {root:true});
            //dispatch('employer/profile/setProfile','', {root:true});
            commit(AUTH_MUTATIONS.SET_TOKEN, data)
            await dispatch('employer/profile/setProfile',null, {root:true});

            if (rootState.employer.newJob.product_code) {
                this.$router.push(this.localePath('employer-new-job'))
            } else {
                this.$router.push(this.localePath('employer-dashboard'))
            }

            dispatch('overlayLoader/toggleOverlayLoader', false, {root:true});
            // dispatch('mainDialog/autDialog',{ type: 'loginSuccess' }, {root:true});
        } catch(e) {
            //open success dialog
            if (e.response && [401, 403, 404].indexOf(e.response.status) > -1) {
                dispatch('mainDialog/autDialog',{ type: e.response.status }, {root:true});
            } else {
                dispatch('mainDialog/autDialog',{ type: '400' }, {root:true});
            }
        }
    },

    // za debugovanje
    setT({ commit }) {
        commit(AUTH_MUTATIONS.SET_TOKEN, {
            token:"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2NTY2NzY2NTgsImV4cCI6MTY1Njc2MzA1OCwiZW1wX2lkIjoiMjM1MiJ9._IwAX7pMlRsOmsUJNo1GZtBuPC8jdS5L-hUDfj3D-PI",
            refresh_token:"b3ab058e40336db90a4a3a30c58e0622",
            expires:"1234566"
        })
    },

    async register ({ commit, dispatch }, payload) {
        try {
 //           console.log("id=API_61d34b53b05076.87317770")
            const {data: data} = await this.$axios.post('/api/public/register_company', payload)
            dispatch('mainDialog/regDialog',{ type: 'regSuccess', email: payload.user_email }, {root:true});
            this.$router.push(this.localePath('/'))
        } catch(e) {
            dispatch('mainDialog/regDialog',{ type: 'regFail', errors: e.response.data.errors }, {root:true});
        }
    },

    async confirmRegistration ({ commit, dispatch, rootState }, id) {

        try {
            const {data: data} = await this.$axios.get('/api/employer_token/verify/'+id)
            dispatch('common/setUserType', 'employer', {root:true});
            commit(AUTH_MUTATIONS.SET_TOKEN, data)

            await dispatch('employer/profile/setProfile',null, {root:true});

            if (rootState.employer.newJob.product_code) {
                this.$router.push(this.localePath('employer-new-job'))
            } else {
                this.$router.push(this.localePath('employer-dashboard'))
            }

            dispatch('mainDialog/regDialog',{ type: 'confRegSuccess' }, {root:true});
        } catch(e) {
            console.log("ERR 1", e.response)
            console.log("ERR 2", e)
            this.$router.push(this.localePath('/'))
            dispatch('mainDialog/regDialog',{ type: 'confRegFail' }, {root:true});
        }
    },

    // given the current refresh token, refresh the user's access token to prevent expiry
    async refresh ({ commit, state }) {
        const {data: data} = await this.$axios.post('/api/employer_token/refresh', state)
        commit(AUTH_MUTATIONS.SET_TOKEN, data)
    },

    // logout the user
    logout ({ commit, dispatch }) {
        // dispatch('mainDialog/autDialog',{ type: 'logoutSuccess' }, {root:true});
        dispatch('common/setUserType', null, {root:true});
        dispatch('employer/profile/reset', {}, {root:true});
        commit(AUTH_MUTATIONS.LOGOUT)
        this.$router.push(this.localePath('/'));
    },
}
