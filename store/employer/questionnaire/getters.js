export default {
    getAllQuestions (state) {
        return state.questions
    },

    getCheckedQuestions (state) {
        return state.questions.filter(question => question.checked)
    }
}