export const initialQuestions = () => {
    return [
        {
            checked:false,
            type:'checkbox',
            title:"Koliko prethodnog radnog iskustva imate na istoj ili sličnim pozicijama?",
            answers:[
                {satisfying:false, title:'Nemam iskustva'},
                {satisfying:false, title:'Do 1 godine'},
                {satisfying:false, title:'1 do 2 godine'},
                {satisfying:false, title:'2 do 5 godina'},
                {satisfying:false, title:'Više od 5 godina'},
            ]
        },
        {
            checked:false,
            type:'checkbox',
            title:"Koliko prethodnog radnog iskustva imate u [[NAVEDITE OBLAST]]?",
            answers:[
                {satisfying:false, title:'Nemam iskustva'},
                {satisfying:false, title:'Do 1 godine'},
                {satisfying:false, title:'1 do 2 godine'},
                {satisfying:false, title:'2 do 5 godina'},
                {satisfying:false, title:'Više od 5 godina'},
            ]
        },
        {
            checked:false,
            type:'checkbox',
            title:"Navedite kategorije vozačke dozvole koje posedujete?",
            answers:[
                {satisfying:false, title:'A'},
                {satisfying:false, title:'B'},
                {satisfying:false, title:'C'},
                {satisfying:false, title:'E'},
                {satisfying:false, title:'D'},
                {satisfying:false, title:'Ni jednu'},
            ]
        },
        {
            checked:false,
            type:'checkbox',
            title:"Stepen stručne spreme?",
            answers:[
                {satisfying:false, title:"I Stepen - četiri razreda osnovne"},
                {satisfying:false, title:"II Stepen - osnovna škola"},
                {satisfying:false, title:"III Stepen - SSS srednja škola (3 godine)"},
                {satisfying:false, title:"IV Stepen - SSS srednja škola (4 godine)"},
                {satisfying:false, title:"V Stepen - VKV - SSS srednja škola"},
                {satisfying:false, title:"VI Stepen - VS viša škola"},
                {satisfying:false, title:"VII Stepen - VSS visoka stručna sprema"},
                {satisfying:false, title:"VII-1 Stepen - Specijalista"},
                {satisfying:false, title:"VII-2 Stepen - Magistratura"},
                {satisfying:false, title:"VIII Stepen - Doktorat"},
            ]
        },
        {
            checked:false,
            type:'checkbox',
            title:"Ocenite Vaše poznavanje rada u MS Word-u i Excel-u. ",
            answers:[
                {satisfying:false, title:"Ne poznajem"},
                {satisfying:false, title:"Početni nivo"},
                {satisfying:false, title:"Srednji nivo"},
                {satisfying:false, title:"Napredni nivo"},
                {satisfying:false, title:"Ekspertski nivo"},
            ]
        },
        {
            checked:false,
            type:'checkbox',
            title:"Ocenite Vaše znanje engleskog jezika. ",
            answers:[
                {satisfying:false, title:"Ne poznajem"},
                {satisfying:false, title:"Početni nivo"},
                {satisfying:false, title:"Srednji nivo"},
                {satisfying:false, title:"Napredni nivo"},
                {satisfying:false, title:"Ekspertski nivo"},
            ]
        },
        {
            checked:false,
            type:'checkbox',
            title:"Ocenite Vaše poznavanje i upotrebu interneta? ",
            answers:[
                {satisfying:false, title:"Ne poznajem"},
                {satisfying:false, title:"Početni nivo"},
                {satisfying:false, title:"Srednji nivo"},
                {satisfying:false, title:"Napredni nivo"},
                {satisfying:false, title:"Ekspertski nivo"},
            ]
        },
        {
            checked:false,
            type:'checkbox',
            title:"Ocenite Vaše poznavanje rada u Photoshopu. ",
            answers:[
                {satisfying:false, title:"Ne poznajem"},
                {satisfying:false, title:"Početni nivo"},
                {satisfying:false, title:"Srednji nivo"},
                {satisfying:false, title:"Napredni nivo"},
                {satisfying:false, title:"Ekspertski nivo"},
            ]
        },
        {
            checked:false,
            type:'checkbox',
            title:"Ocenite Vaše znanje [[NAVEDITE JEZIK,STRUKU,VEŠTINU]].",
            answers:[
                {satisfying:false, title:"Ne poznajem"},
                {satisfying:false, title:"Početni nivo"},
                {satisfying:false, title:"Srednji nivo"},
                {satisfying:false, title:"Napredni nivo"},
                {satisfying:false, title:"Ekspertski nivo"},
            ]
        },
        {
            checked:false,
            type:'checkbox',
            title:"Da li posedujete vozačku dozvolu B kategorije? ",
            answers:[
                {satisfying:false, title:"Da"},
                {satisfying:false, title:"Ne"},
            ]
        },
        {
            checked:false,
            type:'text',
            title:"Šta Vas čini idealnim kandidatom za ovu poziciju?",
            answers:[
                {satisfying:false, title:"TEXT"},
            ]
        },
        {
            checked:false,
            type:'checkbox',
            title:"Konkurišete za poziciju u:",
            answers:[
                {satisfying:false, title:"[[Navedite mesto]]"},
                {satisfying:false, title:"[[Navedite mesto]]"},
            ]
        },
        {
            checked:false,
            type:'checkbox',
            title:"Konkurišete za poziciju:",
            answers:[
                {satisfying:false, title:"[[Navedite poziciju]]"},
                {satisfying:false, title:"[[Navedite poziciju]]"},
            ]
        },

    ]
}

export default () => ({
    questions: initialQuestions()
})