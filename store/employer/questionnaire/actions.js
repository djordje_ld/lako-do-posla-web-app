import {QUESTIONNAIRE_MUTATIONS} from "./mutations";

export default {

    setQuestion ({ commit, dispatch }, {questionIndex, question}) {
        commit(QUESTIONNAIRE_MUTATIONS.SET_QUESTION, {questionIndex, question})
    },

    checkQuestion ({ commit, dispatch }, {questionIndex, checked}) {
        commit(QUESTIONNAIRE_MUTATIONS.CHECK_QUESTION, {questionIndex, checked})
    },

    checkAnswer ({ commit, dispatch }, {questionIndex, answerIndex, checked}) {
        commit(QUESTIONNAIRE_MUTATIONS.CHECK_ANSWER, {questionIndex, answerIndex, checked})
    },

    deleteQuestion ({ commit, dispatch }, index) {
        commit(QUESTIONNAIRE_MUTATIONS.DELETE_QUESTION, index)
    },

    addNewQuestion ({ commit, dispatch }) {
        commit(QUESTIONNAIRE_MUTATIONS.ADD_NEW_QUESTION)
    },

    setQuestionnaireFromOldJob ({ commit }, questionnaire ) {
        for (let questionIndex in questionnaire) {
            questionnaire[questionIndex].checked = true;
            delete(questionnaire[questionIndex].id)
        }

        commit(QUESTIONNAIRE_MUTATIONS.SET_QUESTIONNAIRE_FROM_OLD_JOB, questionnaire)
    },

    async setDefault ({ commit }) {
        commit(QUESTIONNAIRE_MUTATIONS.SET_DEFAULT)
    },
}
