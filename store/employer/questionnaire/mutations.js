import {initialQuestions} from "@/store/employer/questionnaire/state";

export const QUESTIONNAIRE_MUTATIONS = {
    CHECK_QUESTION: 'CHECK_QUESTION',
    CHECK_ANSWER: 'CHECK_ANSWER',
    DELETE_QUESTION: 'DELETE_QUESTION',
    SET_QUESTION: 'SET_QUESTION',
    ADD_NEW_QUESTION: 'ADD_NEW_QUESTION',
    SET_QUESTIONNAIRE_FROM_OLD_JOB: 'SET_QUESTIONNAIRE_FROM_OLD_JOB',
    SET_DEFAULT: 'SET_DEFAULT',
};

export default {
    [QUESTIONNAIRE_MUTATIONS.SET_QUESTION] (state, {questionIndex, question}) {
        state.questions[questionIndex].type = question.type;
        state.questions[questionIndex].title = question.title;
        state.questions[questionIndex].answers = question.answers;
    },

    [QUESTIONNAIRE_MUTATIONS.CHECK_QUESTION] (state, {questionIndex, checked}) {
        state.questions[questionIndex].checked = checked;
    },

    [QUESTIONNAIRE_MUTATIONS.CHECK_ANSWER] (state, {questionIndex, answerIndex, checked}) {
        state.questions[questionIndex]['answers'][answerIndex]['satisfying'] = checked;
    },

    [QUESTIONNAIRE_MUTATIONS.DELETE_QUESTION] (state, index) {
        state.questions.splice(index, 1);
    },

    [QUESTIONNAIRE_MUTATIONS.ADD_NEW_QUESTION] (state) {
        state.questions.push({
            checked:false,
            type:'text',
            title:"[[ PITANJE ]]",
            answers:[]
        })
    },

    [QUESTIONNAIRE_MUTATIONS.SET_QUESTIONNAIRE_FROM_OLD_JOB] (state, questionnaire) {
        questionnaire.map(question => {
            state.questions.push(question)
        })
    },

    [QUESTIONNAIRE_MUTATIONS.SET_DEFAULT] (state) {
        state.questions = {};
        state.questions = initialQuestions();
    }
}