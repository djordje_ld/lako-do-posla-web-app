import {SEARCH_PARAMS_MUTATIONS} from "./mutations";

export default {
    setSearchParams({commit, state}, searchParamsString){ //let's say searchParamsString = search_term=kuvar&page=2
        commit(SEARCH_PARAMS_MUTATIONS.SET_SEARCH_PARAMS_STRING, searchParamsString)
    },

    setSearchObjectProp({commit}, {prop, value}) {
        commit(SEARCH_PARAMS_MUTATIONS.SET_PROP, {prop, value})
    },

    resetSearch({commit}) {
        commit(SEARCH_PARAMS_MUTATIONS.RESET_SEARCH)
    }
}
