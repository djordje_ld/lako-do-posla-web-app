export default {
    isOpened(state) {
        return state.open;
    },
    getSearchParamsString(state) {
        let objectKeys = Object.keys(state);
        let objectValues = Object.values(state);
        let searchString = [];
        for (let i in objectKeys) {
            searchString.push(objectKeys[i] + '=' + objectValues[i]);
        }
        return searchString.join('&');
    },

    getSearchParamsObject(state) {
        return state;
    }
}