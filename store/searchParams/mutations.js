// reusable aliases for mutations
export const SEARCH_PARAMS_MUTATIONS = {
    SET_PROP: 'SET_PROP',
    SET_SEARCH_PARAMS_STRING: 'SET_SEARCH_PARAMS_STRING',
    RESET_SEARCH: 'RESET_SEARCH',
}
export default {
    [SEARCH_PARAMS_MUTATIONS.SET_PROP] (state,  {prop, value}) {
        //only one value allowed (radio)
        if (prop == 'date_range' || prop == 'search_term' || prop == 'page' || prop == 'emp_id' || prop == 'contract_types' || prop == 'sort') {
            
            state[prop]= state[prop] == String(value) ? '' : value
            return;
        }
        // multiple values allowed (checkbox)
        if (prop == 'regions') {
            return state[prop] = value;
        }

        let selectedData = (state[prop] == '') ?  [] : state[prop].split(',');
        let isset = false;
        selectedData.map(data => {
            if (String(data) == String(value)) isset = true;
        })

        if (isset) {
            selectedData = selectedData.filter(data => {
                return String(data) != String(value);
            })
        } else {
            selectedData.push(value)
        }
        state[prop] = selectedData.sort().join(',')
    },

    [SEARCH_PARAMS_MUTATIONS.SET_SEARCH_PARAMS_STRING] (state,  searchParamsString) {
        if (searchParamsString) {
            let explodedParams = searchParamsString.split('&'); // you'll get array // [search_term=kuvar, page=2]


            for (let i in explodedParams) { //loop  through [search_term=kuvar, page=2]
                let explodedValues = explodedParams[i].split('='); // you'll get [search_term, kuvar]
                state[explodedValues[0]] = explodedValues[1] //it literally searchParamsObject.value.search_term = kuvar
            }
        }
    },
    [SEARCH_PARAMS_MUTATIONS.RESET_SEARCH] (state) {
                state.emp_id = '';
                state.regions = '';
                state.categories = '';
                state.search_term = '';
                state.career_levels = '';
                state.education_levels = '';
                state.date_range = '';
                state.page = 1;
    }
}