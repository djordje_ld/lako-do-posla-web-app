export default () => ({
    emp_id: '',
    regions: '',
    categories: '',
    search_term: '',
    career_levels: '',
    education_levels: '',
    contract_types: '',
    date_range: '',
    page: 1,
    sort: '',
})