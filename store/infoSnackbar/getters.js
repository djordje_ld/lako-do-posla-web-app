export default {
    isOpened(state) {
        return state.open;
    },
    getColor(state) {
        return state.color;
    },
    getText(state) {
        return state.text;
    }
}