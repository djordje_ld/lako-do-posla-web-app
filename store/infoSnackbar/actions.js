import {INFO_SNACKBAR_MUTATIONS} from "./mutations";

export default {
    toggleInfoSnackbar({commit,dispatch}, payload) {
        commit(INFO_SNACKBAR_MUTATIONS.TOGGLE, payload)
    }
}
