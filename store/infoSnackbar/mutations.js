// reusable aliases for mutations
export const INFO_SNACKBAR_MUTATIONS = {
    TOGGLE: 'TOGGLE_INFO_SNACKBAR',
}
export default {
    //main dialog mutations
    [INFO_SNACKBAR_MUTATIONS.TOGGLE] (state, payload) {
        state.open = payload.open; // this is required
        state.text = payload.text ? payload.text : state.text;
        state.color = payload.color ? payload.color : state.color;
    }
}