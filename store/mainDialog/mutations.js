// reusable aliases for mutations
export const MAIN_DIALOG_MUTATIONS = {
    TOGGLE: 'MAIN_DIALOG_TOGGLE',
    SET_CONTENT: 'MAIN_DIALOG_SET_CONTENT',
    SET_RELOAD_AFTER_CLOSE: 'SET_RELOAD_AFTER_CLOSE',
}
export default {
    //main dialog mutations
    [MAIN_DIALOG_MUTATIONS.TOGGLE] (state, payload) {
        state.open = payload;
    },
    [MAIN_DIALOG_MUTATIONS.SET_CONTENT] (state, {color, title, body}) {
        state.content.color = color
        state.content.title = title
        state.content.body = body
    },
    [MAIN_DIALOG_MUTATIONS.SET_RELOAD_AFTER_CLOSE]  (state, payload) {
        state.reload_after_close = payload;
    }

}