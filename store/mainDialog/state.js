export default () => ({
    open: false,
    content: {
        color: '',
        title: '',
        body: '',
    },
    redirect_after_close:null,
    reload_after_close:false,
})