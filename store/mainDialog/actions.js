import {MAIN_DIALOG_MUTATIONS} from "./mutations";

export default {
    setMainDialogContent({commit}, {color, title, body}) {
        commit(MAIN_DIALOG_MUTATIONS.SET_CONTENT, {color, title, body})
    },
    toggleMainDialog({commit, state}, payload) {
        commit(MAIN_DIALOG_MUTATIONS.TOGGLE, payload)
        if (!payload && state.reload_after_close) {
            window.location.reload();
        }
    },
    setReloadAfterClose({commit}, payload) {
        commit(MAIN_DIALOG_MUTATIONS.SET_RELOAD_AFTER_CLOSE, payload)
    },
    autDialog ({ commit, dispatch }, { type }) {
        let types = {
            400: {
                color: 'red',
                title:this.$i18n.t('mainDialogContent.login400.title'),
                body:this.$i18n.t('mainDialogContent.login400.body')
            },
            401: {
                color: 'red',
                title:this.$i18n.t('mainDialogContent.login401.title'),
                body:this.$i18n.t('mainDialogContent.login401.body')
            },
            404: {
                color: 'red',
                title:this.$i18n.t('mainDialogContent.login404.title'),
                body:this.$i18n.t('mainDialogContent.login404.body')
            },
            403: {
                color: 'red',
                title:this.$i18n.t('mainDialogContent.login403.title'),
                body:this.$i18n.t('mainDialogContent.login403.body')
            },
            loginSuccess : {
                color: 'green',
                title:this.$i18n.t('mainDialogContent.loginSuccess.title'),
                body:this.$i18n.t('mainDialogContent.loginSuccess.body')
            },
            loginFail : {
                color: 'red',
                title:this.$i18n.t('mainDialogContent.loginFail.title'),
                body:this.$i18n.t('mainDialogContent.loginFail.body')
            },
            logoutSuccess : {
                color: 'blue',
                title:this.$i18n.t('mainDialogContent.logoutSuccess.title'),
                body:this.$i18n.t('mainDialogContent.logoutSuccess.body')
            },

        };
        let content = types[type];

        dispatch('setMainDialogContent', content);
        dispatch('toggleMainDialog', true);
    },

    regDialog ({ commit, dispatch }, payload) {
        let types = {
            regSuccess : {
                color: 'green',
                title:this.$i18n.t('mainDialogContent.registrationSuccess.title'),
                body:this.$i18n.t('mainDialogContent.registrationSuccess.body')
            },
            regFail : {
                color: 'red',
                title:this.$i18n.t('mainDialogContent.registrationFail.title'),
                body:this.$i18n.t('mainDialogContent.registrationFail.body')
            },
            confRegSuccess : {
                color: 'green',
                title:this.$i18n.t('mainDialogContent.confirmationSuccess.title'),
                body:this.$i18n.t('mainDialogContent.confirmationSuccess.body')
            },
            confRegFail : {
                color: 'red',
                title:this.$i18n.t('mainDialogContent.confirmationFail.title'),
                body:this.$i18n.t('mainDialogContent.confirmationFail.body')
            },
        };

        if (payload.type === 'regFail') {
            for (let error in payload.errors) {
                types.regFail.body += payload.errors[error];
            }
        } else if (payload.type === 'regSuccess') {
            types.regSuccess.body += ' '+ payload.email;
        }


        let content = types[payload.type];
        dispatch('setMainDialogContent', { ...content });
        dispatch('toggleMainDialog', true);
    },

    deleteDocs ({ commit, dispatch }, docs) {
        let body = docs + '?';
        dispatch('setMainDialogContent', {
            color: 'red',
            title:this.$i18n.t('mainDialogContent.deleteFiles'),
            body: body
        });
        dispatch('toggleMainDialog', true);
    },

    profileUpdated ({ commit, dispatch }, successfullyUpdated) {
        let cnt = {};
        if (successfullyUpdated) {
            cnt = {
                color: 'green',
                title:this.$i18n.t('mainDialogContent.updateProfileSuccess.title'),
                body:this.$i18n.t('mainDialogContent.updateProfileSuccess.body')
            }
        } else {
            cnt = {
                color: 'red',
                title:this.$i18n.t('mainDialogContent.updateProfileFail.title'),
                body:this.$i18n.t('mainDialogContent.updateProfileFail.body')
            }
        }

        dispatch('setMainDialogContent', { ...cnt });
        dispatch('toggleMainDialog', true);
    },

    profileDeleted ({ commit, dispatch }) {
        dispatch('setMainDialogContent', {
            color: 'blue',
            title:this.$i18n.t('mainDialogContent.profileDeleted.title'),
            body:this.$i18n.t('mainDialogContent.profileDeleted.body')
        });
        dispatch('toggleMainDialog', true);
    },

    profileNotDeleted ({ commit, dispatch }) {
        dispatch('setMainDialogContent', {
            color: 'red',
            title:"Nalog nije obrisan!",
            body: "Doslo je do greske, nalog nije obrisan!"
        });
        dispatch('toggleMainDialog', true);
    },

    jobPosted ({ commit, dispatch }, [posted, additionalText]) {
        let cnt = {};
        if (posted) {
            cnt = {
                color: 'green',
                title:this.$i18n.t('mainDialogContent.jobPosted.success.title'),
                body:this.$i18n.t('mainDialogContent.jobPosted.success.body')
            }
        } else {
            cnt = {
                color: 'red',
                title:this.$i18n.t('mainDialogContent.jobPosted.fail.title'),
                body:this.$i18n.t('mainDialogContent.jobPosted.fail.body') + additionalText
            }
        }

        dispatch('setMainDialogContent', { ...cnt });
        dispatch('toggleMainDialog', true);
    },


    applicationSuccess ({ commit, dispatch }) {
        let cnt = {};
        cnt = {
            color: 'green',
            title:this.$i18n.t('mainDialogContent.applicationSuccess.title'),
            body:this.$i18n.t('mainDialogContent.applicationSuccess.body')+` <a href="${this.localePath('candidate-applications')}">${this.$i18n.t('links.myApplications')}</a>`
        }

        dispatch('setMainDialogContent', { ...cnt });
        dispatch('toggleMainDialog', true);
    },


    applicationFail ({ commit, dispatch }) {
        let cnt = {};
        cnt = {
            color: 'red',
            title:this.$i18n.t('mainDialogContent.applicationFail.title'),
            body:this.$i18n.t('mainDialogContent.applicationFail.body')
        }
        dispatch('setMainDialogContent', { ...cnt });
        dispatch('toggleMainDialog', true);
    },


    documentUploaded ({ commit, dispatch }) {
        let cnt = {};
        cnt = {
            color: 'green',
            title: this.app.i18n.t('general.documentAdded'),//TO-DO translate
            body:""
        }

        dispatch('setMainDialogContent', { ...cnt });
        dispatch('toggleMainDialog', true);
    },
    documentNotUploaded ({ commit, dispatch }) {
        let cnt = {};
        cnt = {
            color: 'red',
            title:this.$i18n.t('mainDialogContent.jobPosted.fail.title'),
            body:'',
        }

        dispatch('setMainDialogContent', { ...cnt });
        dispatch('toggleMainDialog', true);
    },

    documentDeleted ({ commit, dispatch }) {
        let cnt = {
            color: 'green',
            title: this.app.i18n.t('general.selectedDocumentsDeleted'),
            body:""
        }

        dispatch('setMainDialogContent', { ...cnt });
        dispatch('toggleMainDialog', true);
    },

    documentNotDeleted ({ commit, dispatch }) {
        let cnt = {
            color: 'red',
            title:this.$i18n.t('mainDialogContent.documentDeleteFail.title'),
            body:""
        }

        dispatch('setMainDialogContent', { ...cnt });
        dispatch('toggleMainDialog', true);
    },

    emailSentToApplicants ({ commit, dispatch }) {
        dispatch('setMainDialogContent', {
            color: 'blue',
            title:this.$i18n.t('mainDialogContent.emailSentToApplicants.title'),
            body:this.$i18n.t('mainDialogContent.emailSentToApplicants.body')
        });
        dispatch('toggleMainDialog', true);
    },

    emailNotSentToApplicants ({ commit, dispatch }) {
        dispatch('setMainDialogContent', {
            color: 'red',
            title:this.$i18n.t('mainDialogContent.emailNotSentToApplicants.title'),
            body:this.$i18n.t('mainDialogContent.emailNotSentToApplicants.body')
        });
        dispatch('toggleMainDialog', true);
    },

    resetPasswordEmailSent ({ commit, dispatch }, email) {
        let cnt = {};
        cnt = {
            color: 'green',
            //title: this.app.i18n.t('general.documentAdded'),//TO-DO translate
            title:this.$i18n.t('mainDialogContent.resetPasswordEmailSent.title'),
            body:+this.$i18n.t('mainDialogContent.resetPasswordEmailSent.body')+" "+email+". "+this.$i18n.t('checkYourMailBox')
        }

        dispatch('setMainDialogContent', { ...cnt });
        dispatch('toggleMainDialog', true);
    },

    resetPasswordEmailNotSent ({ commit, dispatch }) {
        let cnt = {};
        cnt = {
            color: 'red',
            title:this.$i18n.t('mainDialogContent.resetPasswordEmailNotSent.title'),
            body:this.$i18n.t('mainDialogContent.resetPasswordEmailNotSent.body')
        }

        dispatch('setMainDialogContent', { ...cnt });
        dispatch('toggleMainDialog', true);
    },

    resetPasswordSuccess ({ commit, dispatch }) {
        let cnt = {};
        cnt = {
            color: 'green',
            title:this.$i18n.t('mainDialogContent.resetPasswordSuccess.title'),
            body:this.$i18n.t('mainDialogContent.resetPasswordSuccess.body')
        }

        dispatch('setMainDialogContent', { ...cnt });
        dispatch('toggleMainDialog', true);
    },

    // resetPasswordFail ({ commit, dispatch }) {
    //     let cnt = {};
    //     cnt = {
    //         color: 'red',
    //         title:this.$i18n.t('mainDialogContent.resetPasswordFail.title'),
    //         body:this.$i18n.t('mainDialogContent.resetPasswordFail.body')
    //     }
    //
    //     dispatch('setMainDialogContent', { ...cnt });
    //     dispatch('toggleMainDialog', true);
    // },

    resetPasswordFail ({ commit, dispatch }, { type }) {
        let types = {
            400: {
                color: 'red',
                title:this.$i18n.t('mainDialogContent.resetPassword400.title'),
                body:this.$i18n.t('mainDialogContent.resetPassword400.body')
            },
            404: {
                color: 'red',
                title:this.$i18n.t('mainDialogContent.resetPassword404.title'),
                body:this.$i18n.t('mainDialogContent.resetPassword404.body')
            },
            403: {
                color: 'red',
                title:this.$i18n.t('mainDialogContent.resetPassword403.title'),
                body:this.$i18n.t('mainDialogContent.resetPassword403.body')
            }

        };
        let content = types[type];

        dispatch('setMainDialogContent', content);
        dispatch('toggleMainDialog', true);
    },

    promoImageChanged ({ commit, dispatch }) {
        let cnt = {};
        cnt = {
            color: 'green',
            title:this.$i18n.t('mainDialogContent.promoImageChanged.title'),
            body:this.$i18n.t('mainDialogContent.promoImageChanged.body')
        }

        dispatch('setMainDialogContent', { ...cnt });
        dispatch('toggleMainDialog', true);
    },

    promoImageNotChanged ({ commit, dispatch }) {
        let cnt = {};
        cnt = {
            color: 'green',
            title:this.$i18n.t('mainDialogContent.promoImageNotChanged.title'),
            body:this.$i18n.t('mainDialogContent.promoImageNotChanged.body')
        }

        dispatch('setMainDialogContent', { ...cnt });
        dispatch('toggleMainDialog', true);
    },
}
