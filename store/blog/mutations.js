// reusable aliases for mutations
export const ADMIN_BLOG_MUTATIONS = {
    TEST: 'TEST',
}
export default {
    [ADMIN_BLOG_MUTATIONS.TEST] (state, { id, email_address }) {
        state.id = id
        state.email_address = email_address
    }
}