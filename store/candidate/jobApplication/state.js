export default () => ({
    documents: [],
    ldp_cv:false,
    profile_data: false,
    questionnaire: {
        required: true,
        questions: [],
        answers: []
    },
    old_applications: []
})