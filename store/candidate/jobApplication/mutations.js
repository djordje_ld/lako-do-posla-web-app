// reusable aliases for mutations
export const JOB_APPLICATION_MUTATIONS = {
    SET_FILES: 'SET_FILES',
    SET_LDP_CV: 'SET_LDP_CV',
    SET_PROFILE_DAT: 'SET_PROFILE_DAT',
    SET_QUESTIONNAIRE: 'SET_QUESTIONNAIRE',
    SET_QUESTIONNAIRE_QUESTIONS: 'SET_QUESTIONNAIRE_QUESTIONS',
    SET_ANSWERS: 'SET_ANSWERS',
    RESET_ANSWERS: 'RESET_ANSWERS',
    CHECK_LDP_CV: 'CHECK_LDP_CV',
    CHECK_PROFILE_DATA: 'CHECK_PROFILE_DATA',
    RESET_CHECK_LDP_CV: 'RESET_CHECK_LDP_CV',
    SET_OLD_APPLICATIONS: 'SET_OLD_APPLICATIONS',
    EXPAND_DETAILS: 'EXPAND_DETAILS',
}
export default {
    [JOB_APPLICATION_MUTATIONS.RESET_ANSWERS] (state) {
        state.questionnaire.answers = [];
    },
    [JOB_APPLICATION_MUTATIONS.SET_QUESTIONNAIRE] (state, payload) {
        state.questionnaire.required = payload.length > 0
        state.questionnaire.questions = payload
    },
    [JOB_APPLICATION_MUTATIONS.SET_ANSWERS] (state, {questionId, type, answer}) {
        let found = false;

        state.questionnaire.answers.map((answerObject, index) => {
            if (answerObject.id == questionId) {
                found = true;
                if (type === 'radio') {
                    state.questionnaire.answers[index].selected_options = [answer];
                }
                if (type === 'text') {
                    state.questionnaire.answers[index].text = answer;
                    if (!state.questionnaire.answers[index].text) {
                        state.questionnaire.answers = state.questionnaire.answers.filter(a => a.id != questionId)
                    }
                }
                if (type === 'checkbox') {
                    let indexOfSelectedOption = answerObject.selected_options.indexOf(answer);

                    if (indexOfSelectedOption > -1) {
                        state.questionnaire.answers[index].selected_options = state.questionnaire.answers[index].selected_options.filter(id => id != answer )
                        if (!state.questionnaire.answers[index].selected_options.length) {
                            state.questionnaire.answers = state.questionnaire.answers.filter(a => a.id != questionId)
                        }
                    } else {
                        state.questionnaire.answers[index].selected_options.push(answer);
                    }

                }
            }

        });

        if (found) return;

        if (type === 'radio' || type === 'checkbox') {
            state.questionnaire.answers.push({
                id:questionId,
                selected_options:[answer]
            })
        } else {
            state.questionnaire.answers.push({
                id:questionId,
                text:answer
            });
        }
    },

    [JOB_APPLICATION_MUTATIONS.CHECK_LDP_CV] (state, status) {
        state.ldp_cv = status;
    },

    [JOB_APPLICATION_MUTATIONS.CHECK_PROFILE_DATA] (state, status) {
        state.profile_data = status;
    },

    [JOB_APPLICATION_MUTATIONS.RESET_CHECK_LDP_CV] (state) {
        state.ldp_cv = false;
    },

    [JOB_APPLICATION_MUTATIONS.SET_OLD_APPLICATIONS] (state, payload) {
        state.old_applications = payload;
    },

    [JOB_APPLICATION_MUTATIONS.EXPAND_DETAILS] (state, {status, appIndex}) {
        state.old_applications[appIndex] = status;
    },
}