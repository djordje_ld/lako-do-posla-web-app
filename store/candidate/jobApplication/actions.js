import {JOB_APPLICATION_MUTATIONS} from "./mutations";

export default {
    async setQuestionnaire ({ commit }, jobId) {
        commit(JOB_APPLICATION_MUTATIONS.RESET_ANSWERS)
        commit(JOB_APPLICATION_MUTATIONS.RESET_CHECK_LDP_CV)
        this.$axios.get('/api/public/questions/'+jobId)
            .then(result => {
                commit(JOB_APPLICATION_MUTATIONS.SET_QUESTIONNAIRE, result.data)
            })
    },

    setAnswer ({ commit, state }, {questionId, type, answer}) {
        commit(JOB_APPLICATION_MUTATIONS.SET_ANSWERS, {questionId, type, answer})
    },


    checkLdpCv ({ commit, state }) {
        commit(JOB_APPLICATION_MUTATIONS.CHECK_LDP_CV)
    },

    checkProfileData ({ commit, state }, status) {
        commit(JOB_APPLICATION_MUTATIONS.CHECK_PROFILE_DATA, status)
    },

    checkCv ({ commit, state }, status) {
        commit(JOB_APPLICATION_MUTATIONS.CHECK_LDP_CV, status)
    },

    async apply ({ rootGetters, state, dispatch }, jobId) {

        let application = {
            files: rootGetters['candidate/documents/getSelectedDocumentIds'],
            ldp_cv: state.ldp_cv,
            profile_data: state.profile_data,
            questions: state.questionnaire.answers,
        }
        this.$axios.post('/api/user/apply/'+jobId, application, {
            headers: {
                'content-type': 'application/json'
            }
        })
        .then(result => {
            dispatch('mainDialog/applicationSuccess', {}, {root:true})
            this.$router.push(this.localePath({ name: 'job-details-id', params: { id:jobId  } }))
        }) .catch(e => {
            dispatch('mainDialog/applicationFail', {}, {root:true})
            dispatch('setOldApplications')
            this.$router.push(this.localePath({ name: 'job-details-id', params: { id:jobId  } }))
        })
    },

    async setOldApplications ({ rootGetters, state, commit }, all = false) {
        let recent = (all) ? '' : '/recent'

        return new Promise(resolve => {
            this.$axios.get('/api/user/applications'+recent, {
                headers: {
                    'content-type': 'application/json'
                }
            })
                .then(result => {
                    for (let i in result.data) {
                        result.data[i].expand_details = false;
                    }
                    commit(JOB_APPLICATION_MUTATIONS.SET_OLD_APPLICATIONS, result.data)
                    resolve(result.data)
                }) .catch(e => resolve(e))
        })

    },

    async expandDetails ({state, commit }, {status, appIndex}) {
        commit(JOB_APPLICATION_MUTATIONS.EXPAND_DETAILS,  {status, appIndex})
    }

}
