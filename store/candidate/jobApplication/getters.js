export default {
    questionnaireGood(state, context) {
        return state.questionnaire.required && false/// is questionnaire good;
    },

    allQuestionsAnswered(state, context) {
        return state.questionnaire.questions.length === state.questionnaire.answers.length
    },

    allAnswers(state, context) {
        return state.questionnaire.answers
    },

    allQuestions(state, context) {
        return state.questionnaire.questions
    },

    ldpCvChecked(state, context) {
        return state.ldp_cv
    },
    profileDataChecked(state, context) {
        return state.profile_data
    },
    getApplications(state, context) {
        return state.old_applications
    }
}