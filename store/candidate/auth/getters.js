export default {
  isAuthenticated(state, context) {
    return state.token !== null;
  }
}