export default () => ({
    token: null, // JWT access token
    refresh_token: null, // JWT refresh token
    expires: null, // JWT expires timestamp
})