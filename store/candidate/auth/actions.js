import {AUTH_MUTATIONS} from "./mutations";

export default {
    async login ({ commit, dispatch, rootState }, { email, password }) {
        try {

            const {data: data} = await this.$axios.post('/api/token', {email, password })
            dispatch('common/setUserType', 'candidate', {root:true});
            await commit(AUTH_MUTATIONS.SET_TOKEN, data)
            await dispatch('candidate/profile/get', {}, {root:true});


            let route = (rootState.common.jobWantedToApply) ? this.localePath({name: 'job-details-id',params: {id: rootState.common.jobWantedToApply}}) : '/';
            this.$router.push(route);

            // dispatch('mainDialog/autDialog',{ type: 'loginSuccess' }, {root:true});

        } catch(e) {
            //open success dialog
            if (e.response && [401, 403, 404].indexOf(e.response.status) > -1) {
                dispatch('mainDialog/autDialog',{ type: e.response.status }, {root:true});
            } else {
                dispatch('mainDialog/autDialog',{ type: '400' }, {root:true});
            }
        }
    },

    async register ({ commit, dispatch }, payload) {
        try {
            const {data: data} = await this.$axios.post('/api/public/register', payload)
            dispatch('mainDialog/regDialog',{ type: 'regSuccess', email: payload.user_email }, {root:true});
            this.$router.push(this.localePath('/'))
        } catch(e) {
            dispatch('mainDialog/regDialog',{ type: 'regFail', errors: e.response.data.errors }, {root:true});
        }
    },

    // given the current refresh token, refresh the user's access token to prevent expiry
    async refresh ({ commit, state }) {
        const {data: data} = await this.$axios.post('/api/token/refresh', state)
        commit(AUTH_MUTATIONS.SET_TOKEN, data)
    },


    async socialLogin ({ commit, dispatch, rootState }, payload) {
        try {
            const {data: data} = await this.$axios.post('/api/token/provider', payload, {
                header:{
                    "Content-Type" : "application/json"
                }
            })
            dispatch('common/setUserType', 'candidate', {root:true});
            await commit(AUTH_MUTATIONS.SET_TOKEN, data)
            await dispatch('candidate/profile/get', {}, {root:true});


            let route = (rootState.common.jobWantedToApply) ? this.localePath({name: 'job-details-id',params: {id: rootState.common.jobWantedToApply}}) : '/';
            this.$router.push(route);

            dispatch('mainDialog/autDialog',{ type: 'loginSuccess' }, {root:true});

        } catch(e) {
            dispatch('mainDialog/autDialog',{ type: 'loginFail' }, {root:true});
        }
    },

    async confirmRegistration ({ commit, dispatch, rootState }, id) {

        try {
            const data = await this.$axios.get('/api/token/verify/'+id)
            console.log({data});
            dispatch('common/setUserType', 'candidate', {root:true});
            commit(AUTH_MUTATIONS.SET_TOKEN, data.data)
            
            console.log("DAATRAAA", data)
            await dispatch('candidate/profile/get', {}, {root:true});

            let route = (rootState.common.jobWantedToApply) ? this.localePath({name: 'job-details-id',params: {id: rootState.common.jobWantedToApply}}) : '/';
            this.$router.push(this.localePath(route))
            dispatch('mainDialog/regDialog',{ type: 'confRegSuccess' }, {root:true});
        } catch(e) {
            this.$router.push(this.localePath('/'))
            dispatch('mainDialog/regDialog',{ type: 'confRegFail' }, {root:true});
        }
    },

    // logout the user
    logout ({ commit, dispatch }) {
        dispatch('common/setUserType', null, {root:true});
        commit(AUTH_MUTATIONS.LOGOUT)
        dispatch('candidate/profile/reset', {}, {root:true});
        this.$router.push(this.localePath('/'));
        // dispatch('mainDialog/autDialog',{ type: 'logoutSuccess' }, {root:true});
    }
}
