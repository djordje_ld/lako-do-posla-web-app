import {SEARCHES_MUTATIONS} from "./mutations";

export default {
    async get ({ commit, dispatch }, payload) {
        const {data: data} = await this.$axios.get('/api/user/searches');
        commit(SEARCHES_MUTATIONS.SET_SEARCHES, data.reverse());
    },

    delete ({ commit, dispatch }, searchId) {
        this.$axios.delete('api/user/searches/'+searchId)
            .then(response => {
                commit(SEARCHES_MUTATIONS.DELETE, searchId);
            })
    },

    async save ({ commit, dispatch }, payload) {
        this.$axios.post('/api/user/searches',JSON.stringify(payload),{
            headers: {
                'Content-Type' : 'application/json'
            }
        })
        .then(response => {
            delete(payload.notification_status);
            commit(SEARCHES_MUTATIONS.ADD_SEARCH, payload);
            dispatch('recentSearches/delete', payload, {root:true})
            dispatch('get')
        })
    },

    updateNotification ({ commit, dispatch }, {searchId, status}) {
        dispatch('get')
    },
}
