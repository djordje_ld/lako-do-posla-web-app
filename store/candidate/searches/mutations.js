// reusable aliases for mutations
export const SEARCHES_MUTATIONS = {
    SET_SEARCHES: 'SET_SEARCHES',
    DELETE: 'DELETE',
    ADD_SEARCH: 'ADD_SEARCH',
    UPDATE_NOTIFICATION: 'UPDATE_NOTIFICATION',
}
export default {
    [SEARCHES_MUTATIONS.SET_SEARCHES] (state, payload) {
        let searches = [];
        payload.map(search => {
             let keys = Object.keys(search.search);
             keys.map (key => {
                 search.search[key] = search.search[key] == "0" ? "" : search.search[key];
             })
             searches.push(search)
        });
        state.data = searches
    },

    [SEARCHES_MUTATIONS.DELETE] (state, searchId) {
        let data = state.data.filter(search => {
            return search.id != searchId
        })

        state.data = data.reverse();
    },

    [SEARCHES_MUTATIONS.ADD_SEARCH] (state, search) {
        state.data = [search, ...state.data];
    },

    [SEARCHES_MUTATIONS.UPDATE_NOTIFICATION] (state, {searchId, status}) {
        state.data.map(search => {
            if (searchId == search.id) {
                search.notification_status = status
            }
        })
    },
}