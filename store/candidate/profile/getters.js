export default {
  getCvScore(state, context) {
     return state.data.cv_score;
  },

  cvReady(state, context) {
    return state.data.cv_score > 80;
  }
}