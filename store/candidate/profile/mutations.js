export const PROFILE_MUTATIONS = {
    SET_PROFILE: 'SET_PROFILE',
    UPDATE_PROFILE: 'UPDATE_PROFILE',
    SET_PROFILE_IMAGE: 'SET_PROFILE_IMAGE',
}
export default {
    [PROFILE_MUTATIONS.SET_PROFILE] (state, profile) {
        state.data = {...profile}
    },

    [PROFILE_MUTATIONS.SET_PROFILE_IMAGE] (state, {logo, logo_path}) {
        state.data.logo = logo;
        state.data.logo_path = logo_path;
    }
}