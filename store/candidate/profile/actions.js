import {PROFILE_MUTATIONS} from "./mutations";

export default {
    async get({commit}) {
        return this.$axios.get('/api/user/data')
            .then(response => {
                commit(PROFILE_MUTATIONS.SET_PROFILE, response.data);
                return response.data;
            }).catch(e =>console.log(e.response))
    },

    reset ({ commit, dispatch }) {
        commit(PROFILE_MUTATIONS.SET_PROFILE, {})
    },

    async update({commit, dispatch}, dataToUpdate) {
        await this.$axios.patch(
            'api/user/data/profile',
            JSON.stringify(dataToUpdate),
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(response => {
                commit(PROFILE_MUTATIONS.SET_PROFILE, response.data);
                dispatch('mainDialog/profileUpdated', true, {root: true})
            })
            .catch(error => {
                dispatch('mainDialog/profileUpdated', false, {root: true})
            })
    },

    async setProfileImage({commit}, file) {
        let formData = new FormData();
        formData.append('user_image', file, file.name);
        return new Promise(resolve => {
            this.$axios.post(
                'api/user/image',
                formData, {
                    headers: {
                        'Content-Type': 'multipart/formdata'
                    }
                })
                .then(async response => {
                    resolve(
                        commit(PROFILE_MUTATIONS.SET_PROFILE_IMAGE, {
                            logo: response.data.logo,
                            logo_path: response.data.logo_path
                        })
                    );
                }).then(error => resolve(error))
        })

    },

    deleteAccount({dispatch}) {
        this.$axios.delete('api/user/delete_account')
            .then(response => {
                dispatch('candidate/auth/logout', true, {root: true})
                dispatch('mainDialog/profileDeleted', false, {root: true})
            })
            .catch(error => {
                dispatch('mainDialog/profileNotDeleted', false, {root: true})
            })
    },

}
