export const SAVED_JOBS_MUTATIONS = {
    ADD: 'ADD',
    DELETE: 'DELETE',
    RESET: 'RESET',
};
export default {
    [SAVED_JOBS_MUTATIONS.ADD] (state, job) {
        state.jobs.push({
            id: job.id,
            thumb: job.logo_path,
            title: job.title,
            short_message: job.short_message,
            company: job.company,
            regions: job.regions,
            expires: job.expires,
            product_code: job.product_code,
            promo_image: job.promo_image
        });
    },

    [SAVED_JOBS_MUTATIONS.DELETE] (state, jobId) {

        let newState = state.jobs.filter(job => {
            return job.id != jobId
        });

        state.jobs = newState;

    },

    [SAVED_JOBS_MUTATIONS.RESET] (state) {
        state.jobs = []
    }
}