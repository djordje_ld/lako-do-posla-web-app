export default {
  savedJobsIds(state, context) {
    let ids = [];
    state.jobs.map(job => ids.push(job.id))
    return ids;
  },

  getSavedJobs(state) {
    return state.jobs;
  },
}