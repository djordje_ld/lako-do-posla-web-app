import {SAVED_JOBS_MUTATIONS} from "./mutations";

export default {

    add ({ commit }, job) {
        commit(SAVED_JOBS_MUTATIONS.ADD, job);
    },

    delete ({ commit }, jobId) {
        commit(SAVED_JOBS_MUTATIONS.DELETE, jobId);
    },

    reset ({ commit }, jobId) {
        commit(SAVED_JOBS_MUTATIONS.RESET);
    },
}
