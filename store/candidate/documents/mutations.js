// reusable aliases for mutations
export const DOCUMENTS_MUTATIONS = {
    SET_DOCUMENTS: 'SET_DOCUMENTS',
    SET_DOCUMENT: 'SET_DOCUMENT',
    DELETE_DOCUMENTS: 'DELETE_DOCUMENTS',
    SELECT_DOCUMENT: 'SELECT_DOCUMENT',
}
export default {
    [DOCUMENTS_MUTATIONS.SET_DOCUMENTS] (state, payload) {
        state.documents = [];
        payload.map(doc => {
            state.documents.push({
                selected: false,
                ...doc
            })
        })
    },

    [DOCUMENTS_MUTATIONS.SET_DOCUMENT] (state, document) {
        document.selected = false;
        state.documents.push(document)
    },

    [DOCUMENTS_MUTATIONS.DELETE_DOCUMENTS] (state, ids) {
        // let newState = [];
        // for (let i in state.documents) {
        //     if (ids.indexOf(state.documents[i].file_id) == -1) {
        //         newState.push(state.documents[i])
        //     }
        // }
        //
        //
        // state.documents = newState;
        state.documents = state.documents.filter((document) => {
            if (ids.indexOf(document.file_id) == -1) {
                return document
            }
        })
    },

    [DOCUMENTS_MUTATIONS.SELECT_DOCUMENT] (state, documentId) {
        state.documents = state.documents.map((document) => {
            document.selected = (document.file_id === documentId) ? !document.selected : document.selected
            return document
        })
    },
}