import {DOCUMENTS_MUTATIONS} from "./mutations";

export default {
    async get ({ commit, dispatch }, payload) {
        const {data: data} = await this.$axios.get('/api/user/documents');
        commit(DOCUMENTS_MUTATIONS.SET_DOCUMENTS, data);
        return data;
    },

    async deleteMultiple ({ commit, state, dispatch }) {
        let selected = state.documents.map(document => {
            if (document.selected) return document.file_id
        });

        selected = selected.filter(documentId => {
            if (documentId) return documentId
        });

        if (selected.length) {
            dispatch('delete', selected.join(','))
        }

    },


    async delete ({ commit, state, dispatch }, commaSeparatedIds) {
        this.$axios.delete('/api/user/documents/'+commaSeparatedIds)
            .then(succ => {
                commit(DOCUMENTS_MUTATIONS.DELETE_DOCUMENTS, commaSeparatedIds.split(','));
                dispatch('mainDialog/documentDeleted', {}, {root:true})
            })
            .catch(e => {
                dispatch('mainDialog/documentNotDeleted', {}, {root:true})
            })
        },

    async upload ({ commit,dispatch }, payload) {
        return new Promise(resolve => {
            this.$axios.post('/api/user/documents', payload)
                .then(succ => {
                    commit(DOCUMENTS_MUTATIONS.SET_DOCUMENT, succ.data);
                    dispatch('mainDialog/documentUploaded', '',{root:true})
                    resolve()
                })
                .catch(() => {
                    dispatch('mainDialog/documentNotUploaded', '',{root:true})
                    resolve()
                })
        })

    },

    async select ({ commit }, documentId) {
        commit(DOCUMENTS_MUTATIONS.SELECT_DOCUMENT, documentId);
    },

    async download ({state}, doc) {
        const response = this.$axios.get('/api/user/document/'+doc.file_id, { responseType: "blob" })
            .then(response => {
                const blob = new Blob([response.data], { type: doc.file_type });
                const link = document.createElement("a");
                link.href = URL.createObjectURL(blob);
                link.download = doc.file_name;
                link.click();
                URL.revokeObjectURL(link.href);
            })
    },

    async downloadMultiple ({state, dispatch}) {
        state.documents.map(doc => {
            if (doc.selected) {
                dispatch('download', doc)
            }
        });
    },

    async downloadCv ({state}) {
        const response = this.$axios.get('/api/user/document/CV', { responseType: "blob" })
            .then(response => {
                const blob = new Blob([response.data], { type: 'application/pdf' });
                const link = document.createElement("a");
                link.href = URL.createObjectURL(blob);
                link.download = "Lako do posla CV";
                link.click();
                URL.revokeObjectURL(link.href);
            })
    },

    async downloadProfileData ({state}) {
        const response = this.$axios.get('/api/user/document/PROFILE', { responseType: "blob" })
            .then(response => {
                const blob = new Blob([response.data], { type: 'application/pdf' });
                const link = document.createElement("a");
                link.href = URL.createObjectURL(blob);
                link.download = "Lako do posla Profil";
                link.click();
                URL.revokeObjectURL(link.href);
            })
    },
    
}
