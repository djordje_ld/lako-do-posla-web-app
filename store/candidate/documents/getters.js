export default {
  getSelectedDocuments(state, context) {
    return state.documents.filter(document => {
      if (document.selected) return document;
    })
  },

  getSelectedDocumentIds(state, context) {
    let selected = [];
    state.documents.map(document => {
      if (document.selected) selected.push(document.file_id);
    })
    return selected;
  },

  getReversed(state, context) {
    let reversed = [...state.documents];
    return reversed.reverse();

  }


}