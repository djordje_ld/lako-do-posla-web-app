export default () => ({
    theme_dark: false,
    user_type: null, // candidate or employer
    categories: null,
    contacts: null,
    main_dialog : {
        open: false,
        content: {
            color: '',
            title: '',
            body: '',
        },
        redirect_after_close:null,
    },
    generalData: {

    },
    jobWantedToApply: null,
    jobJustCreated: false,
})