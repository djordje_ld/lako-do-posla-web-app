import {COMMON_MUTATIONS} from "./mutations";

export default {
    setUserType({commit, dispatch}, userType) {
        commit(COMMON_MUTATIONS.SET_USER_TYPE, userType)
    },
    async setCategories({commit}, categories) {
        const {data} = await this.$axios.get('api/public/categories');
        commit(COMMON_MUTATIONS.SET_CATEGORIES, data)
    },
    setDarkTheme({commit}) {
        commit(COMMON_MUTATIONS.SET_DARK_THEME)
    },

    setGeneralData({commit}) {
        return new Promise (async (resolve) => {
            const {data: settings} = await this.$axios.get('api/public/settings');
            const {data: posting_prices} = await this.$axios.get('api/public/posting_prices');
            const {data: employers} = await this.$axios.get('api/public/active_employers');
            const {data: contacts} = await this.$axios.get('api/public/contacts');
            const {data: posting_prices_advanced} = await this.$axios.get('api/public/posting_prices_adv');
            const {data: posting_packages} = await this.$axios.get('api/public/posting_packages');
            const {data: posting_description} = await this.$axios.get('api/public/posting_description');


            let data = {
                ...settings,
                posting_prices,
                employers,
                contacts,
                posting_prices_advanced,
                posting_packages,
                posting_description,
                published: [
                    {
                        id: '1',
                        name:'Najnovije' //translate
                    },
                    {
                        id: '2',
                        name:'U poslednja 3 dana'//translate
                    }
                ]
            }
            commit(COMMON_MUTATIONS.SET_GENERAL_DATA, data)
            return resolve(data);
        })
    },

    setJobWantedToApply({commit}, jobId) {
        commit(COMMON_MUTATIONS.SET_JOB_WANTED_TO_APPLY, jobId)
    },

    setJobJustCreated({commit}, status) {
        commit(COMMON_MUTATIONS.SET_JOB_JUST_CREATED, status)
    }
}