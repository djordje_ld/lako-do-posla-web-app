export default {
    isDarkTheme(state) {
        return state.theme_dark;
    }
}