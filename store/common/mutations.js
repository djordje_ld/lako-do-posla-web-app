// reusable aliases for mutations
export const COMMON_MUTATIONS = {
    SET_USER_TYPE: 'SET_USER_TYPE',
    SET_CATEGORIES: 'SET_CATEGORIES',
    SET_DARK_THEME: 'SET_DARK_THEME',
    SET_GENERAL_DATA: 'SET_GENERAL_DATA',
    SET_JOB_WANTED_TO_APPLY: 'SET_JOB_WANTED_TO_APPLY',
    SET_JOB_JUST_CREATED: 'SET_JOB_JUST_CREATED',
}
export default {
    // store the logged in user in the state
    [COMMON_MUTATIONS.SET_USER_TYPE] (state, user_type) {
        state.user_type = user_type
    },

    //job categories
    [COMMON_MUTATIONS.SET_CATEGORIES] (state, payload) {
        state.categories = payload
    },

    //dark theme
    [COMMON_MUTATIONS.SET_DARK_THEME] (state, payload) {
        state.theme_dark = !state.theme_dark
    },

    //general data  (categories, educations, locations...)
    [COMMON_MUTATIONS.SET_GENERAL_DATA] (state, data) {
        state.generalData = data;
    },

    [COMMON_MUTATIONS.SET_JOB_WANTED_TO_APPLY] (state, jobId) {
        state.jobWantedToApply = jobId;
    },

    [COMMON_MUTATIONS.SET_JOB_JUST_CREATED] (state, status) {
        state.jobJustCreated = status;
    }

}