// reusable aliases for mutations
export const CLOSE_SELECT_MENU_MUTATIONS = {
    TOGGLE_BTN: 'TOGGLE_BTN',
}
export default {
    //main dialog mutations
    [CLOSE_SELECT_MENU_MUTATIONS.TOGGLE_BTN] (state, payload) {
        state.status = payload;
    },

}