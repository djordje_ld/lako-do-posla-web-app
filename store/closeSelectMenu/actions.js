import {CLOSE_SELECT_MENU_MUTATIONS} from "./mutations";

// you should pass a content to dialog, like:

export default {
    toggleBtn({commit}, payload) {
        commit(CLOSE_SELECT_MENU_MUTATIONS.TOGGLE_BTN, payload)
    },
}
