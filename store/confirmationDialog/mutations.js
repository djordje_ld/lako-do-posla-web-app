// reusable aliases for mutations
export const CONFIRMATION_DIALOG_MUTATIONS = {
    TOGGLE: 'TOGGLE_CONFIRMATION_DIALOG',
    SET_CONTENT: 'SET_CONTENT_CONFIRMATION_DIALOG',
}
export default {
    //main dialog mutations
    [CONFIRMATION_DIALOG_MUTATIONS.TOGGLE] (state, payload) {
        state.status = payload;
    },
    [CONFIRMATION_DIALOG_MUTATIONS.SET_CONTENT] (state, payload) {
        state.content.title = payload.content.title
        state.content.body = payload.content.body
        state.content.buttons.confirm = payload.content.buttons.confirm
        state.content.buttons.decline = payload.content.buttons.decline

        state.confirmAction = payload.confirmAction;
        state.confirmPayload = payload.confirmPayload;
        // you can pass decline action/payload but you don't have to (it will just close the confirmation dialog)
        state.declineAction = payload.declineAction ? payload.declineAction : '';
        state.declinePayload = payload.declinePayload ? payload.declinePayload : '';
    },

}