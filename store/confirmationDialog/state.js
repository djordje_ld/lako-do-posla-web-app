export default () => ({
    status: false,
    content: {
        title: 'hello',
        body: '',
        buttons: {
            confirm: '',
            decline: ''
        }
    },
    confirmAction: '',
    confirmPayload: null,
    declineAction: '',
    declinePayload: null,
})