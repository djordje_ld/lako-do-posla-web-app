import {CONFIRMATION_DIALOG_MUTATIONS} from "./mutations";

// you should pass a content to dialog, like:

export default {
    setConfirmationDialogContent({commit}, payload) {
        commit(CONFIRMATION_DIALOG_MUTATIONS.SET_CONTENT, payload)
    },
    toggleConfirmationDialog({commit}, payload) {
        commit(CONFIRMATION_DIALOG_MUTATIONS.TOGGLE, payload)
    },

    confirmAction({dispatch}, {confirmAction, confirmPayload}) {
        dispatch(confirmAction, confirmPayload, {root:true})
    },

    async saveProfileData ({commit, dispatch}, {confirmAction, confirmPayload}) {
        let content = {
            title: this.$i18n.t('confirmationDialogContent.saveProfileData.title'),
            body: this.$i18n.t('confirmationDialogContent.saveProfileData.body'),
            buttons: {
                confirm: this.$i18n.t('confirmationDialogContent.saveProfileData.buttons.confirm'),
                decline: this.$i18n.t('confirmationDialogContent.saveProfileData.buttons.decline'),
            }
        }
        await dispatch('setConfirmationDialogContent', {
            content,
            confirmAction,
            confirmPayload
        });
        dispatch('toggleConfirmationDialog', true);
        dispatch('mainDialog/setReloadAfterClose', true, {root:true});
    },

    deleteDocumentConfirmation ({commit, dispatch},  {confirmAction, confirmPayload}) {
        let content = {
            title: 'Obrisi sve oblezena dokumenta',
            body: 'Da li ste sigurni da zelite da obriste obelezena dokumenta ',
            buttons: {
                confirm: 'Da, obrisi',
                decline: 'Odustani'
            }
        }
        dispatch('setConfirmationDialogContent', {
            content,
            confirmAction,
            confirmPayload
        });
        dispatch('toggleConfirmationDialog', true);
    },

    changeProfileImageConfirmation ({commit, dispatch},  {confirmAction, confirmPayload}) {
        let content = {
            title: this.$i18n.t('confirmationDialogContent.changeProfileImage.title'),
            body: this.$i18n.t('confirmationDialogContent.changeProfileImage.body'),
            buttons: {
                confirm: this.$i18n.t('confirmationDialogContent.changeProfileImage.buttons.confirm'),
                decline: this.$i18n.t('confirmationDialogContent.changeProfileImage.buttons.decline'),
            }
        }
        dispatch('setConfirmationDialogContent', {
            content,
            confirmAction,
            confirmPayload
        });
        dispatch('toggleConfirmationDialog', true);
    },
    // uploadJobPromoImageConfirmation ({commit, dispatch},  {confirmAction, confirmPayload}) {
    //     let content = {
    //         title: this.$i18n.t('confirmationDialogContent.uploadJobPromoImage.title'),
    //         body: this.$i18n.t('confirmationDialogContent.uploadJobPromoImage.body'),
    //         buttons: {
    //             confirm: this.$i18n.t('confirmationDialogContent.uploadJobPromoImage.buttons.confirm'),
    //             decline: this.$i18n.t('confirmationDialogContent.uploadJobPromoImage.buttons.decline'),
    //         }
    //     }
    //     return new Promise(async resolve => {
    //         dispatch('toggleConfirmationDialog', true);
    //         await dispatch('setConfirmationDialogContent', {
    //             content,
    //             confirmAction,
    //             confirmPayload
    //         });
    //
    //         resolve();
    //     })
    // },
    deleteSearchConfirmation ({commit, dispatch},  {confirmAction, confirmPayload}) {
        let content = {
            // title: this.$i18n.t('confirmationDialogContent.changeProfileImage.title'),
            // body: this.$i18n.t('confirmationDialogContent.changeProfileImage.body'),
            // buttons: {
            //     confirm: this.$i18n.t('confirmationDialogContent.changeProfileImage.buttons.confirm'),
            //     decline: this.$i18n.t('confirmationDialogContent.changeProfileImage.buttons.decline'),
            // } // TO-DO: translate
            title: "Paznja!",
            body: "Da li zaista zelite da obrisete pretragu?",
            buttons: {
                confirm: this.$i18n.t('confirmationDialogContent.changeProfileImage.buttons.confirm'),
                decline: this.$i18n.t('confirmationDialogContent.changeProfileImage.buttons.decline'),
            }
        }
        dispatch('setConfirmationDialogContent', {
            content,
            confirmAction,
            confirmPayload
        });
        dispatch('toggleConfirmationDialog', true);
    },
    activateNotificationForSavedSearch ({commit, dispatch},  {confirmAction, confirmPayload, declineAction, declinePayload}) {
        let content = {
            // title: this.$i18n.t('confirmationDialogContent.changeProfileImage.title'),
            // body: this.$i18n.t('confirmationDialogContent.changeProfileImage.body'),
            // buttons: {
            //     confirm: this.$i18n.t('confirmationDialogContent.changeProfileImage.buttons.confirm'),
            //     decline: this.$i18n.t('confirmationDialogContent.changeProfileImage.buttons.decline'),
            // }
            title: "Aktivirajte obavestenja!",
            body: "Aktivirajte obavestenja kako biste bili obavesteni kada se pojavi konkurs sa kreterijumima ove pretrage.",
            buttons: {
                // confirm: this.$i18n.t('confirmationDialogContent.changeProfileImage.buttons.confirm'),
                // decline: this.$i18n.t('confirmationDialogContent.changeProfileImage.buttons.decline'),
                confirm: "Aktiviraj i sacuvaj",
                decline: 'Samo sacuvaj',
            }
        }
        dispatch('setConfirmationDialogContent', {
            content,
            confirmAction,
            confirmPayload,
            declineAction,
            declinePayload,
        });
        dispatch('toggleConfirmationDialog', true);
    },

    deleteAccount ({commit, dispatch},  {confirmAction, confirmPayload}) {
        let content = {
            // TO-DO: translate //all from this page!!!!
            // title: this.$i18n.t('confirmationDialogContent.changeProfileImage.title'),
            // body: this.$i18n.t('confirmationDialogContent.changeProfileImage.body'),
            // buttons: {
            //     confirm: this.$i18n.t('confirmationDialogContent.changeProfileImage.buttons.confirm'),
            //     decline: this.$i18n.t('confirmationDialogContent.changeProfileImage.buttons.decline'),
            // }
            title: "Pažnja!",
            body: "Vaš nalog će biti obrisan! Da li ste sigurni da želite da obrišete vaš nalog?",
            buttons: {
                // confirm: this.$i18n.t('confirmationDialogContent.changeProfileImage.buttons.confirm'),
                // decline: this.$i18n.t('confirmationDialogContent.changeProfileImage.buttons.decline'),
                confirm: "Da, obriši",
                decline: 'Odustani',
            }
        }

        dispatch('setConfirmationDialogContent', {
            content,
            confirmAction,
            confirmPayload
        });
        dispatch('toggleConfirmationDialog', true);
    },

}
