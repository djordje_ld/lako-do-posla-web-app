// reusable aliases for mutations
export const RECENT_SEARCHES_MUTATIONS = {
    SET: 'SET',
    SET_DEFAULT: 'SET_DEFAULT',
    DELETE: 'DELETE',
}
export default {
    [RECENT_SEARCHES_MUTATIONS.SET] (state, search) {
        if (state.data > 10) state.data = state.data.slice(0,10);
        state.data.push(search)
        state.data = state.data.reverse();
        localStorage.setItem('recentSearches', JSON.stringify(state.data));
    },

    [RECENT_SEARCHES_MUTATIONS.SET_DEFAULT] (state, searches) {
        // if (state.data > 10) delete(state.data[0])
        state.data = [...searches];
    },

    [RECENT_SEARCHES_MUTATIONS.DELETE] (state, index) {
        state.data = state.data.filter((search,i) => {
            if (index != i) {
                return search;
            }
        })
    }
}