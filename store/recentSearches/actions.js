import {RECENT_SEARCHES_MUTATIONS} from "./mutations";

export default {
    async set ({ commit, dispatch, state, rootState }, search) {
        if(rootState.employer.auth.token) return;

        let recentSearches = JSON.parse(localStorage.getItem('recentSearches'));
        if (!recentSearches) return;

        let equal = false;

        recentSearches.map(recentSearch => {
            if (_.isEqual(recentSearch, search)) {
                equal = true;
            }
        })

        if (rootState.candidate.auth.token) {
            rootState.candidate.searches.data.map(savedSearch=> {
                if (_.isEqual(savedSearch.search, search)) {
                    equal = true;
                }
            })
        }


        if (!equal) {
            const emptySearch = _.values(search).every(_.isEmpty);
            if (!emptySearch) {
                commit(RECENT_SEARCHES_MUTATIONS.SET, search);
            }
        }
    },

    async setDefault ({ commit}) {
        if (!localStorage.getItem('recentSearches')) {
            localStorage.setItem('recentSearches', JSON.stringify([]))
        }
        let searches = JSON.parse(localStorage.getItem('recentSearches'))
        commit(RECENT_SEARCHES_MUTATIONS.SET_DEFAULT, searches);
    },

    async delete ({ commit, dispatch, state }, search) {
        let searches = localStorage.getItem('recentSearches') ? JSON.parse(localStorage.getItem('recentSearches')) : []

        let filteredSearches = searches.filter(recentSearch => {
            if (!_.isEqual(recentSearch, search)) {
                return search;
            }
        })
        localStorage.removeItem('recentSearches');
        localStorage.setItem('recentSearches', JSON.stringify(filteredSearches));
        commit(RECENT_SEARCHES_MUTATIONS.SET_DEFAULT, filteredSearches);
    }
}
