import {OVERLAY_LOADER_MUTATIONS} from "./mutations";

export default {
    toggleOverlayLoader({commit}, payload) {
        commit(OVERLAY_LOADER_MUTATIONS.TOGGLE, payload)
    }

}
