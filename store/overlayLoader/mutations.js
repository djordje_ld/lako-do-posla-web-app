// reusable aliases for mutations
export const OVERLAY_LOADER_MUTATIONS = {
    TOGGLE: 'TOGGLE_OVERLAY_LOADER',
}
export default {
    //main dialog mutations
    [OVERLAY_LOADER_MUTATIONS.TOGGLE] (state, payload) {
        state.open = payload;
    }
}