import commonState from './common/state'

const CORE_DATA_MUTATIONS = {
    SET_REGIONS: 'SET_REGIONS'
}

export const state = () => ({
    regions: null,
})
export const modules = {
    'common': commonState
}

export const mutations = {}

export const actions = {
    async nuxtServerInit ({ dispatch, commit, state }) {
        //return dispatch('common/setCategories');
    }
}